# Projet : Intelligence artificielle pour un jeu de poker.




Responsables du projet :

Rodolphe __FILLION__ , Théo __HORELLOU__.

A l'attention de notre tuteur de projet : Mr. Fabien __TEYTAUD__, ainsi que le jury d'examinateurs.

### Rapport de projet :  __[Rapport.pdf](Rapport.pdf)__



# Intégration continue & Doxygen

Ce projet utilise la CI de gitlab pour créer automatiquement la documentation Doxygen. Celle-ci est disponible ici : [Doxygen](https://rodolphe.fillion62.gitlab.io/pokia/).

