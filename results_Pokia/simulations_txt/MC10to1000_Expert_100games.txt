Monte Carlo vs Random :
- Nb games on each simulation = 100
- Sb = 10$ , Bb = 20$ , Start money = 1000$
- Iterations MC [100 to 1000]
MC(10) vs Expert : [39/61]
MC(20) vs Expert : [33/67]
MC(30) vs Expert : [48/52]
MC(40) vs Expert : [42/58]
MC(50) vs Expert : [40/60]
MC(60) vs Expert : [49/51]
MC(70) vs Expert : [59/41]
MC(80) vs Expert : [50/50]
MC(90) vs Expert : [41/59]
MC(100) vs Expert : [46/54]
MC(200) vs Expert : [59/41]
MC(300) vs Expert : [49/51]
MC(400) vs Expert : [49/51]
MC(500) vs Expert : [54/46]
MC(600) vs Expert : [55/45]
MC(700) vs Expert : [62/38]
MC(800) vs Expert : [55/45]
MC(900) vs Expert : [58/42]
MC(1000) vs Expert : [51/49]


