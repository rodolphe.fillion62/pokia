
cmake_minimum_required(VERSION 3.0)
project(Pokia)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -O2 -Wall -Wextra")

find_package(PkgConfig REQUIRED)
pkg_check_modules(PKG_CPPUTEST REQUIRED cpputest)
include_directories(${PKG_CPPUTEST_INCLUDE_DIRS})


add_executable(Pokia
        src/main/main.cpp
        src/main/Cards/Box.cpp
        src/main/Cards/Box.hpp
        src/main/Cards/Card.cpp
        src/main/Cards/Card.hpp
        src/main/Game/Board.cpp
        src/main/Game/Board.hpp
        src/main/Players/Player.cpp
        src/main/Players/Player.hpp
        src/main/Game/Game.cpp
        src/main/Game/Game.hpp
        src/main/Cards/EvalFunctions.cpp
        src/main/Cards/EvalFunctions.hpp
        src/main/Players/Random_player.cpp
        src/main/Players/Random_player.hpp
        src/main/Players/All_in_player.cpp
        src/main/Players/All_in_player.hpp
        src/main/Players/Monte_Carlo.cpp
        src/main/Players/Monte_Carlo.hpp
        src/main/Players/Human_player.cpp
        src/main/Players/Human_player.hpp
        src/main/Game/PowerGame.cpp
        src/main/Game/PowerGame.hpp
        src/main/Players/Expert.cpp
        src/main/Players/Expert.hpp
        src/main/Players/Expert_Env.cpp
        src/main/Players/Expert_Env.hpp)


#programme de test
add_executable(Pokia_Test
        src/test/main_test.cpp
        src/test/test_combinaisons.cpp
        src/main/Cards/Box.cpp
        src/main/Cards/Box.hpp
        src/main/Cards/Card.cpp
        src/main/Cards/Card.hpp
        src/main/Game/Board.cpp
        src/main/Game/Board.hpp
        src/main/Players/Player.cpp
        src/main/Players/Player.hpp
        src/main/Game/Game.cpp
        src/main/Game/Game.hpp
        src/main/Cards/EvalFunctions.cpp
        src/main/Cards/EvalFunctions.hpp
        src/main/Players/Random_player.cpp
        src/main/Players/Random_player.hpp
        src/test/test_cards.cpp
        src/test/test_scores.cpp
        src/main/Players/All_in_player.cpp
        src/main/Players/All_in_player.hpp
        src/main/Players/Monte_Carlo.cpp
        src/main/Players/Monte_Carlo.hpp
        src/main/Players/Human_player.cpp
        src/main/Players/Human_player.hpp
        src/main/Game/PowerGame.cpp
        src/main/Game/PowerGame.hpp
        src/main/Players/Expert.cpp
        src/main/Players/Expert.hpp
        src/main/Players/Expert_Env.cpp
        src/main/Players/Expert_Env.hpp)
target_link_libraries(Pokia_Test ${PKG_CPPUTEST_LIBRARIES})
