/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "Random_player.hpp"
#include <iostream>
#include "Player.hpp"
#include "../Game/Game.hpp"
#include <chrono>
#include <random>
static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Constructors
Random_IA::Random_IA(double money) : Player(money) {
}
// Override speak
double Random_IA::speak( Game &_g){

    // Display Bet
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[RANDOM-IA " << getNumber() << "] [money=" << getMoney() << "] [choix=";
    }

    // Find possible choices at this time of the game
    std::vector<double> _randomChoices = findPossibleChoices(_g);

    // Make random engine in all the possible choices
    std::uniform_int_distribution<int> _randG(0,_randomChoices.size()-1);

    // Display Choices
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        for (unsigned i=0;i<_randomChoices.size();i++){
            if (_g.isDisplay()){
                std::cout << _randomChoices[i] << "/";
            }
        }
        if (_g.isDisplay()){
            std::cout << "]";
        }
    }

    // Return random choice
    return _randomChoices[_randG(rng)];
}