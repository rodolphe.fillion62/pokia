/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include "All_in_player.hpp"
#include "Player.hpp"
#include "../Game/Game.hpp"

// Constructors
All_in_player::All_in_player(double money) : Player(money) {
}
// Override speak
double All_in_player::speak( Game &_g){
    // Display Bet
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[ALL-IN " << getNumber() << "] [money=" << getMoney() << "] [choix=";
    }

    return getMoney();
}