/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "Mcts.hpp"
#include <iostream>
#include <tuple>
#include <algorithm>
#include "../Game/Game.hpp"
#include <math.h>
#include <assert.h>
// Constructors
Mcts::Mcts(double money) : Player(money) {
}
Mcts::Mcts(double money, int nbSimulations) : Player(money), _nbSimulations(nbSimulations) {}
// Override speak
double Mcts::speak( Game &_g){

    // BUILD nodes TREE
    _nodes.clear();
    _nodes.reserve(_nbSimulations+1);
    _nodes.emplace_back(_g);

    // MCTS simulations
    for (unsigned i=0; i<_nbSimulations; i++){

        // SELECTION
        NodeMcts * ptrCurrentNode = std::addressof(_nodes.front());
        while (not ptrCurrentNode->_childNodes.empty() and ptrCurrentNode->_possibleChoices.empty())
        {
            ptrCurrentNode = computeSelection(ptrCurrentNode);
        }

        // EXPANSION
        if (not ptrCurrentNode->_currentGame.isEndGame())
        {
            double choice = computeExpansion(ptrCurrentNode);
            // CREATE NODE
            Game & g =  ptrCurrentNode->_currentGame;
            _nodes.emplace_back(g,choice,ptrCurrentNode,getNumber());
            // STORE IN PARENT NODE
            NodeMcts * ptrChildNode = std::addressof(_nodes.back());
            ptrCurrentNode->_childNodes.push_back(ptrChildNode);
            // CHILD = CURRENT NODE
            ptrCurrentNode = ptrChildNode;
        }

        // SIMULATION
        double _gains = computeSimulation(ptrCurrentNode);

        // BACKPROPAGATION
        computeBackpropagation(ptrCurrentNode, _gains);
    }

    // GET BEST NODE
    NodeMcts * ptrBestNode = (NodeMcts *)_nodes.front().getBestNode();
    return ptrBestNode->_choice;
}

///////////////
// Node MCTS
////////////////

// Constructor 1
NodeMcts::NodeMcts(  Game &_g) :
        _currentGame(_g),
        _gains(0),
        _nbSims(0),
        _choice(0),
        _numberMC(-1),
        _ptrParentNode(nullptr)
{
    _possibleChoices = _g.getPlayers()[0]->findPossibleChoices(_currentGame);
    _childNodes.reserve(_possibleChoices.size());
}

// Constructor 2
NodeMcts::NodeMcts(Game &_g,double _choice, NodeMcts * ptrParentNode,int _n) :
        _currentGame(_g),
        _gains(0),
        _nbSims(0),
        _numberMC(_n),
        _choice(_choice),
        _ptrParentNode(ptrParentNode)
{
    // Make a copy of the current game, and play the game with the random choice
    // Saving cards :
    std::vector<Card> _envPlayer,_envBoard;
    // Set 2 Cards of Monte Carlo
    _envPlayer = {_g.getPlayers().at(_g.getPlayerWithNumber(_numberMC))->getCards()[0],_g.getPlayers().at(_g.getPlayerWithNumber(_numberMC))->getCards()[1]};
    // Set 3-4-5 Cards of the board
    switch(_g.getPlayers().at(_g.getPlayerWithNumber(_numberMC))->getCards().size()){
        case 5:
            // Flop
            for (int i=0;i<=2;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        case 6:
            // Turn
            for (int i=0;i<=3;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        case 7:
            // River
            for (int i=0;i<=4;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        default:
            break;
    }
    Game *gameTEST = new Game(_g, _envBoard, _envPlayer);
    gameTEST->setIsMcExpert(true);
    gameTEST->setNMc(_numberMC);
    gameTEST->giveMCCards();
    gameTEST->setChoiceMc(_choice);
    gameTEST->start(false);
    _possibleChoices = _g.getPlayers().at(_g.getPlayerWithNumber(_numberMC))->findPossibleChoices(_currentGame);
    _childNodes.reserve(_possibleChoices.size());
}

// Best node
NodeMcts * NodeMcts::getBestNode() const
{
    assert(not _childNodes.empty());
    auto cmpSims = [] (NodeMcts* ptrNode1, NodeMcts* ptrNode2)
    { return ptrNode1->_nbSims < ptrNode2->_nbSims; };
    auto iter = std::max_element(_childNodes.begin(),
                                 _childNodes.end(), cmpSims);
    return *iter;
}

// Win
double NodeMcts::getWinRatio() const
{
    return _nbSims>0 ? _gains/double(_nbSims) : 0.0;
}
///////////////
// Player MCTS
////////////////

//*****************
// SELECTION
//*****************
NodeMcts * Mcts:: computeSelection(NodeMcts * ptrNode){
    assert(not ptrNode->_childNodes.empty());
    //std::cout << "child=" << ptrNode->_childNodes.size();
    NodeMcts * ptrBestNode = nullptr;
    double bestScore = -1;

    for (NodeMcts * ptrChildNode : ptrNode->_childNodes)
    {
        double score = computeScoreUct(ptrChildNode);
        assert(score >= 0);
        if (score > bestScore)
        {
            ptrBestNode = ptrChildNode;
            bestScore = score;
        }
    }
    return ptrBestNode;
}

//*****************
// EXPANSION
//*****************

double Mcts::computeExpansion(NodeMcts * ptrNode){
    double _choice = ptrNode->_possibleChoices.back();
    ptrNode->_possibleChoices.pop_back();
    return _choice;
}

//*****************
// SIMULATION
//*****************

double Mcts::computeSimulation(NodeMcts * ptrNode){
    // MONTE CARLO SIMULATION
    Game _gNow = (ptrNode->_currentGame);

    double _currentMoney = _gNow.getPlayers().at(_gNow.getPlayerWithNumber(getNumber()))->getMoney();
    // Make a copy of the current game, and play the game with the random choice
    // Saving cards :
    std::vector<Card> _envPlayer,_envBoard;
    // Set 2 Cards of Monte Carlo
    _envPlayer = {_gNow.getPlayers().at(_gNow.getPlayerWithNumber(getNumber()))->getCards()[0],_gNow.getPlayers().at(_gNow.getPlayerWithNumber(getNumber()))->getCards()[1]};
    // Set 3-4-5 Cards of the board
    switch(_gNow.getPlayers().at(_gNow.getPlayerWithNumber(getNumber()))->getCards().size()){
        case 5:
            // Flop
            for (int i=0;i<=2;i++) _envBoard.push_back(_gNow.getBoard().getCards()[i]);
            break;
        case 6:
            // Turn
            for (int i=0;i<=3;i++) _envBoard.push_back(_gNow.getBoard().getCards()[i]);
            break;
        case 7:
            // River
            for (int i=0;i<=4;i++) _envBoard.push_back(_gNow.getBoard().getCards()[i]);
            break;
        default:
            break;
    }
    Game *gameTEST = new Game(_gNow, _envBoard, _envPlayer);
    gameTEST->setIsMcExpert(true);
    gameTEST->setNMc(getNumber());
    gameTEST->giveMCCards();
    gameTEST->setChoiceMc(ptrNode->_choice);
    gameTEST->start(false);

    double _gainsPlayers = gameTEST->getPlayers().at(gameTEST->getPlayerWithNumber(getNumber()))->getMoney() - _currentMoney;

    return _gainsPlayers;
}

//*****************
// BACKPROPAGATION
//*****************

void Mcts::computeBackpropagation(NodeMcts * ptrNode, double _gains){
    int _cpt,_nbPlayers;

    _nbPlayers = ptrNode->_currentGame.getNbPlayers();

    _cpt = _nbPlayers-1;
    while (ptrNode)
    {

        for (Player *_p :ptrNode->_currentGame.getPlayers()){
            if ( _p->getNumber()==_cpt ) {
                ptrNode->_gains += _gains;
            }
        }
        _cpt--;

        ptrNode->_nbSims++;
        ptrNode = ptrNode->_ptrParentNode;
    }
}

//*****************
// UCT
//*****************
double Mcts::computeScoreUct(NodeMcts * ptrNode) const{
    double KUCT = 0.25;
    double parentSims = ptrNode->_ptrParentNode->_nbSims;
    double currentSims = ptrNode->_nbSims;
    double exploitation = ptrNode->getWinRatio();
    double exploration = std::sqrt(std::log(parentSims) / currentSims);
    return exploitation + KUCT * exploration;
}
