/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_MCTS_HPP
#define POKIA_MCTS_HPP

#include "Player.hpp"
#include "../Game/Game.hpp"

struct NodeMcts
{
    Game _currentGame;
    double _choice;
    int _numberMC;
    unsigned _gains;
    unsigned _nbSims;
    std::vector<NodeMcts*> _childNodes;
    std::vector<double> _possibleChoices;
    NodeMcts * _ptrParentNode;
    // Constructor 1
    NodeMcts( Game &_g);
    // Constructor 2
    NodeMcts( Game &_g,double _choice, NodeMcts * ptrParentNode, int _n);
    // Best node
    NodeMcts * getBestNode() const;
    // Win Ratio
    double getWinRatio() const;
};

class Mcts : public Player{
private :
    int _nbSimulations;
    std::vector<NodeMcts> _nodes;
public:
    // Constructors
    Mcts(double money);
    Mcts(double money, int nbSimulations);
    // Override speak
    double speak(Game &_g) override;
    // MCTS
    NodeMcts * computeSelection(NodeMcts * ptrNode)  ;
    double computeExpansion(NodeMcts * ptrNode)  ;
    double computeSimulation(NodeMcts * ptrNode)  ;
    void computeBackpropagation(NodeMcts * ptrNode, double _gains);
    // UCT
    double computeScoreUct(NodeMcts * ptrNode) const;
};

#endif
