/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include <tuple>
#include "Player.hpp"
#include "../Game/Game.hpp"

// Constructors
Player::Player(double money) : _money(money) {
    emptyRound();
    _fold = false;
    _leader = false;
    _hb = 0;
    _outOfRound = false;
    _active = true;
    _check = false;
    _smallOrBigBlind = 0;
    _chainAction = "";
    _stackRound = 0;
}
// Getters
int Player::getPoints() const {
    return _points;
}
std::vector<Card> &Player::getCards() {
    return _cards;
}
int Player::getNumber() const {
    return _number;
}
double Player::getMoney() const {
    return _money;
}
double Player::getHb() const {
    return _hb;
}
double Player::getSmallOrBigBlind() const {
    return _smallOrBigBlind;
}
bool Player::isActive() const {
    return _active;
}
bool Player::isFold() const {
    return _fold;
}
bool Player::isLeader() const {
    return _leader;
}
bool Player::isCheck() const {
    return _check;
}
bool Player::isOutOfRound() const {
    return _outOfRound;
}
const std::string &Player::getChainAction() const {
    return _chainAction;
}
bool Player::isDealer() const {
    return _dealer;
}
double Player::getStackRound() const {
    return _stackRound;
}
const std::string &Player::getName() const {
    return _name;
}
double Player::getScoreHands() const {
    return _scoreHands;
}
// Setters
void Player::setSmallOrBigBlind(double smallOrBigBlind) {
    _smallOrBigBlind = smallOrBigBlind;
}
void Player::setOutOfRound(bool outOfRound) {
    _outOfRound = outOfRound;
}
void Player::setHb(double hb) {
    _hb = hb;
}
void Player::setActive(bool active) {
    _active = active;
}
void Player::setMoney(double money) {
    _money = money;
}
void Player::setFold(bool fold) {
    _fold = fold;
}
void Player::setCheck(bool check) {
    _check = check;
}
void Player::setLeader(bool leader) {
    _leader = leader;
}
void Player::setNumber(int number) {
    _number = number;
}
void Player::setCards(const std::vector<Card> &cards) {
    _cards = cards;
}
void Player::setChainAction(const std::string &chainAction) {
    _chainAction = chainAction;
}
void Player::setStackRound(double stackRound) {
    _stackRound = stackRound;
}
void Player::setDealer(bool dealer) {
    _dealer = dealer;
}
void Player::setPoints(int points) {
    _points = points;
}
void Player::setName(const std::string &name) {
    _name = name;
}
void Player::setScoreHands(double scoreHandsTwoCard) {
    _scoreHands = scoreHandsTwoCard;
}
// Methods
double Player::speak(Game &_g) {
    if(getHb() < _g.getHighestBet()){
        return _g.getHighestBet();
    }
    return 0;
}
void Player::emptyRound() {
    _points = 0;
}
void Player::pay(double _amount, Game &_g) {
    if (_money - _amount < 0) {
        _money = 0;
        _g.getBoard().setMoney(_g.getBoard().getMoney() + _money);
    } else {
        _money = _money - _amount;
        _g.getBoard().setMoney(_g.getBoard().getMoney() + _amount);
    }
}
std::vector<double> Player:: findPossibleChoices(Game &_g){

    std::vector<double> _choices;

    // Limitation of the bet to the lowest stack of the game
    double _lowestStack = 9999;
    for (Player *_p : _g.getPlayers()){
        if (!_p->isOutOfRound()){
            if ((_p->getMoney() ) < _lowestStack){
                _lowestStack = _p->getMoney();
            }
        }
    }

    // Bets
    double _gameHB = _g.getHighestBet();
    double _foldOrCheck = 0;
    double _call = _gameHB;
    double _raise = _gameHB + 2 * _g.getBb();

    // Fold Check
    _choices.push_back(_foldOrCheck);

    if ((_lowestStack == 0 && _money >= _call) || ((_lowestStack < _gameHB) && _money >= _call)){
        _choices.push_back(_call);
        return _choices;
    }

    if (_hb < _gameHB) {
        /* CALL */
        if (_lowestStack >= _call) {
            if (_money  >= _gameHB) {
                _choices.push_back(_call);
            }
        }
        /* RAISE */
        if (_lowestStack >= _gameHB) {
            if (_money  >= _gameHB){
                _choices.push_back(_raise);
            }
        }
    }
    else if (_hb == _gameHB){
        /* RAISE */
        if (_lowestStack >= _gameHB) {
            if (_money >= _gameHB){
                _choices.push_back(_raise);
            }
        }
    }

    // Delete possible double 0
    int _cpt=0;
    for (unsigned i=0;i<_choices.size();i++){
        if (_choices[i]==0){
            if (_cpt==2){
                _choices.erase(_choices.begin()+i);
                break;
            }
            else {
                _cpt++;
            }

        }
    }

    // Choice [ALL IN] deleted to gain performances
    /*
    bool _allIN = true;
    // ALL IN
    for (int i=0;i<_choices.size();i++){
        if (_choices[i] == std::min((_money),_lowestStack)){
            _allIN = false;
        }
    }
    if (_allIN){
        if (std::min((_money),_lowestStack) > _gameHB)
            _choices.push_back(std::min((_money),_lowestStack));
    }*/

    return _choices;

}
// Display
void Player::printCards() {
    std::cout<< "";
    _cards[0].printCard();
    _cards[1].printCard();
    std::cout<< "";

}
// Destructor
Player::~Player() {

}










