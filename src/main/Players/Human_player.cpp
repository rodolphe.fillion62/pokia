/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include "Human_player.hpp"
#include "Player.hpp"
#include "../Game/Game.hpp"

// Constructors
Human_player::Human_player(double money) : Player(money) {
}
// Override speak
double Human_player::speak( Game &_g){

    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[HUMAN " << getNumber() << "] [money=" << getMoney() << "] [choix=";
    }

    std::vector<double> _allChoices = findPossibleChoices(_g);
    // Add ALL IN
    if (_allChoices[_allChoices.back()] != getMoney())
    {
        _allChoices.push_back(getMoney());
    }

    double _currentChoice;

    // Iterate all the possible choices
    std::cout << "Possible choices:" << std::endl;
    for (double _d : _allChoices){
        std::cout << "[" << _d << "]" << std::endl;
    }
    std::cin >> _currentChoice;

    // Wrong choice, ask again human
    while (!isValidChoice(_currentChoice,_allChoices)){
        std::cout << "[WRONG CHOICE] , Possible choices:" << std::endl;
        for (double _d : _allChoices){
            std::cout << "[" << _d << "]" << std::endl;
        }
        std::cin >> _currentChoice;
    }

    return _currentChoice;
}