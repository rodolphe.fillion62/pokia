/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_PLAYER_HPP
#define POKIA_PLAYER_HPP


#include <vector>
#include "../Cards/Card.hpp"
#include "../Cards/EvalFunctions.hpp"

class Game;

class Player {
private:
    double _money,_hb,_smallOrBigBlind,_stackRound,_scoreHands;
    int _points,_number;
    bool _fold,_leader,_outOfRound,_active,_check,_dealer;
    std::vector<Card> _cards;
    std::string _name,_chainAction;
public:
    // Constructors
    Player(double money);
    Player(double money, const std::string &name);
    // Getters
    int getPoints() const;
    std::vector<Card> &getCards();
    int getNumber() const;
    double getMoney() const;
    double getHb() const;
    double getSmallOrBigBlind() const;
    bool isFold() const;
    bool isLeader() const;
    bool isOutOfRound() const;
    bool isActive() const;
    bool isCheck() const;
    bool isDealer() const;
    double getStackRound() const;
    const std::string &getChainAction() const;
    const std::string &getName() const;
    double getScoreHands() const;
    // Setters
    void setCards(const std::vector<Card> &cards);
    void setStackRound(double stackRound);
    void setDealer(bool dealer);
    void setNumber(int number);
    void setHb(double hb);
    void setLeader(bool leader);
    void setActive(bool active);
    void setMoney(double money);
    void setCheck(bool check);
    void setFold(bool fold);
    void setOutOfRound(bool outOfRound);
    void setSmallOrBigBlind(double smallOrBigBlind);
    void setChainAction(const std::string &chainAction);
    void setPoints(int points);
    void setName(const std::string &name);
    void setScoreHands(double scoreHandsTwoCard);
    // Display
    void printCards();
    // Methods
    virtual double speak(Game &_g);
    void pay(double _amount, Game &_g);
    void emptyRound();
    std::vector<double> findPossibleChoices(Game &_g);
    // Destructor
    virtual ~Player();
};

#endif
