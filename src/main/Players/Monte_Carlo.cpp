/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include <tuple>
#include "Player.hpp"
#include "Monte_Carlo.hpp"
#include "../Game/Game.hpp"
#include <chrono>
#include <random>

static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Constructors
Monte_Carlo::Monte_Carlo(double money, int nbSimulations) : Player(money), _nbSimulations(nbSimulations) {}
// Override speak
double Monte_Carlo::speak( Game &_g){

    // Saving cards :
    std::vector<Card> _envPlayer,_envBoard;
    // Set 2 Cards of Monte Carlo
    _envPlayer = {getCards()[0],getCards()[1]};
    // Set 3-4-5 Cards of the board
    switch(getCards().size()){
        case 5:
            // Flop
            for (int i=0;i<=2;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        case 6:
            // Turn
            for (int i=0;i<=3;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        case 7:
            // River
            for (int i=0;i<=4;i++) _envBoard.push_back(_g.getBoard().getCards()[i]);
            break;
        default:
            break;
    }

    // Find possible choices at this time of the game
    std::vector<double> _choices = findPossibleChoices(_g);

    // Make random engine in all the possible choices
    std::uniform_int_distribution<int> _randG(0,_choices.size()-1);

    // Vector to save the result of the simulation
    std::vector<double> _rChoices,_rGains,_rNb;

    // Save start money and number of the Monte Carlo player
    int _currentNumber = getNumber();
    double _currentMoney = getMoney();

    // Display Bet
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[MONTE-CARLO " << _currentNumber << "] [money=" << _currentMoney << "] [choix=";
    }

    // If there is only one possible choice, don't simulate Monte Carlo
    if (_choices.size() == 1){
        return _choices[0];
    }

    // Display Choices and initialise saving vectors
    for (unsigned i=0;i<_choices.size();i++){
        if (_g.isDisplay()){
            std::cout << _choices[i] << "/";
        }
        _rGains.push_back(0);
        _rChoices.push_back(_choices[i]);
        _rNb.push_back(0);
    }
    if (_g.isDisplay()){
    std::cout << "]" << std::endl;
    }

    // **********************
    // Simulation Monte Carlo
    // **********************
    for (int i=0;i<_nbSimulations;i++) {

        // Random choice in possible choices
        double _currentChoice = _choices[_randG(rng)];

        // Make a copy of the current game, and play the game with the random choice
        Game *gameTEST = new Game(_g, _envBoard, _envPlayer);
        gameTEST->setIsMcExpert(true);
        gameTEST->setNMc(getNumber());
        gameTEST->giveMCCards();
        gameTEST->setChoiceMc(_currentChoice);
        gameTEST->start(true);

        // Save the money of the Monte Carlo player after this copy game, and calculate benefit
        double _currentGain = gameTEST->getPlayers().at(gameTEST->getPlayerWithNumber(_currentNumber))->getMoney() - _currentMoney;

        // Save the result of the game in vectors
        bool _newChoice = true;
        if (_rChoices.empty()) {
            _rChoices.push_back(_currentChoice);
            _rGains.push_back(_currentGain);
            _rNb.push_back(1);
        } else {
            for (unsigned j = 0; j < _rChoices.size(); j++) {
                if (_rChoices[j] == _currentChoice) {
                    _rGains[j] += _currentGain;
                    _rNb[j]++;
                    _newChoice = false;
                }
            }
            if (_newChoice) {
                _rChoices.push_back(_currentChoice);
                _rGains.push_back(_currentGain);
                _rNb.push_back(1);
            }
        }

        // Uncomment this to see result of each of the simulations
        /*
        std::cout << "[MCADMIN] choice=" << _currentChoice << " gain=" << _currentGain << " ";
        std::cout << "[HISTORY] -> " << gameTEST->getChaineAction() << std::endl;
        */

        // Delete copy game
        delete gameTEST;
    }

    // Find best benefit after all the simulations
    double _bestGain = -99999;
    double _bestChoice = 0;
    for (unsigned i=0;i<_rChoices.size();i++){
        _rGains[i] /= _rNb[i];
        // Display all the choices with their results
        if (_g.isDisplay()){
            std::cout << "   -Choice[" << _rChoices[i] << "] Gains[" << _rGains[i] << "] Nb[" << _rNb[i] << "] " << std::endl;
        }
        // Maximise
        if (_bestGain < _rGains[i]){
            _bestChoice = _rChoices[i];
            _bestGain = _rGains[i];
        }
    }

    // Display Best choice
    if (_g.isDisplay()){
        std::cout << "Best Choice : " << _bestChoice  << std::endl;
        printCards();
    }


    // Return the best choice of _nbSimulations Monte Carlo
    return _bestChoice;
}

