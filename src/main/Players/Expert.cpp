/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "Expert.hpp"
#include <iostream>
#include <tuple>
#include "Player.hpp"
#include "../Game/Game.hpp"
#include <chrono>
#include <random>
#include "../Cards/EvalFunctions.hpp"
#include "../Game/PowerGame.hpp"

static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Constructors
Expert::Expert(double money) : Player(money) {}
// Override speak
double Expert::speak( Game &_g){

    // Display Bet
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[" << getName() << " " << getNumber() << "] [money=" << getMoney() << "] [choix=";
    }

    // Evaluate the hand of Expert
    double _scoreHand = std::get<1>(newGenerateHand(getCards()));

    double _gameHB = _g.getHighestBet();
    double _call = _gameHB;

    // Make random engine in all the possible choices
    std::uniform_int_distribution<int> _randG(0,100);
    int _tmp = _randG(rng);

    double _currentBet=0;

    std::vector<double> _choices = findPossibleChoices(_g);

    // Display Choices
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        for (unsigned i=0;i<_choices.size();i++){
            if (_g.isDisplay()){
                std::cout << _choices[i] << "/";
            }
        }
        if (_g.isDisplay()){
            std::cout << "]";
        }
    }

    // Pre Flop
    if (getCards().size()==2){
        double _evaluationOfHand = searchInTxt(getCards()[0],getCards()[1]);
        if (_g.isDisplay())std::cout << std::endl;
        if (_g.isDisplay())std::cout << "% Win=" << _evaluationOfHand << std::endl;
        if (_g.isDisplay())printCards();
        if (_g.isDisplay())std::cout << std::endl;
        // Raise
        if (_evaluationOfHand > 0.75){
            return _choices.back();
        }
        // Call
        else if ( _evaluationOfHand > 0.55){
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
        // Fold
        else {
            return 0;
        }
        return 0;
    }

    // Hand : Straight or More
    if ( (_scoreHand >= 500000)){
        // Raise
        if (_g.isDisplay())std::cout << "Straight+ -> Raise" << std::endl;
        return _choices.back();
    }
    // Hand : Two Pair Or Three of Kind
    else if (_scoreHand >= 300000 && _scoreHand < 500000){
        // Raise
        if (_tmp > 20){
            if (_g.isDisplay())std::cout << "Two Pair/Tok -> Raise" << std::endl;
            return _choices.back();
        }
        // Call
        else {
            if (_g.isDisplay())std::cout << "Two Pair/Tok -> Call" << std::endl;
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
    }
    // Hand : Pair
    else if ((_scoreHand >= 200000 && _scoreHand < 300000)){
        if (_tmp > 95){
            if (_g.isDisplay())std::cout << "Pair -> Fold" << std::endl;
            _currentBet=0;
        }
        // Call
        else if (_tmp > 47){
            if (_g.isDisplay())std::cout << "Pair -> Call" << std::endl;
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
        // Raise
        else {
            if (_g.isDisplay()) std::cout << "Pair -> Raise" << std::endl;
            return _choices.back();
        }
    }
    // Nothing
    else {
        if (_tmp > 95){
            // Call
            if (_g.isDisplay())std::cout << "Nothing -> Bluff Call" << std::endl;
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
        // Fold
        else {
            if (_g.isDisplay())std::cout << "Nothing -> Fold" << std::endl;
            _currentBet = 0;
        }

    }

    return _currentBet;
}

