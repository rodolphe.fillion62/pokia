/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "Expert_Env.hpp"
#include <iostream>
#include <tuple>
#include "Player.hpp"
#include "../Game/Game.hpp"
#include <chrono>
#include <random>
#include "../Cards/EvalFunctions.hpp"
#include "../Game/PowerGame.hpp"

static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Constructors
Expert_Env::Expert_Env(double money) : Player(money) {}
// Override speak
double Expert_Env::speak( Game &_g){

    // Display Bet
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        std::cout << "[" << getName() << " " << getNumber() << "] [money=" << getMoney() << "] [choix=";
    }

    double _gameHB = _g.getHighestBet();
    double _call = _gameHB;

    double _currentBet=0;

    std::vector<double> _choices = findPossibleChoices(_g);

    // Display Choices
    if (!_g.isIsMcExpert() && _g.isDisplay()){
        for (unsigned i=0;i<_choices.size();i++){
            if (_g.isDisplay()){
                std::cout << _choices[i] << "/";
            }
        }
        if (_g.isDisplay()){
            std::cout << "]";
        }
    }

    // Pre Flop
    if (getCards().size()==2){
        double _evaluationOfHand = searchInTxt(getCards()[0],getCards()[1]);
        if (_g.isDisplay())std::cout << std::endl;
        if (_g.isDisplay())std::cout << "% Win=" << _evaluationOfHand << std::endl;
        if (_g.isDisplay())printCards();
        if (_g.isDisplay())std::cout << std::endl;
        // Raise
        if (_evaluationOfHand > 0.75){
            return _choices.back();
        }
        // Call
        else if ( _evaluationOfHand > 0.55){
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
        // Fold
        else {
            return 0;
        }
        return 0;
    }
    // Post flop
    else {
        double _scoreAllHand = getScoreHands();
        if (_scoreAllHand > 0.8){
            return _choices.back();
        }
        else if (_scoreAllHand > 0.6){
            if (getHb() < _gameHB && getMoney() >= _call){
                return _call;
            }
        }
        else {
            return 0;
        }
    }

    return _currentBet;
}

