/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <vector>
#include <iostream>
#include "Card.hpp"
#include "EvalFunctions.hpp"
#include <tuple>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <random>

static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Data win
#define p_pair  200000
#define p_2pair  300000
#define p_tok  400000
#define p_straight  500000
#define p_flush  600000
#define p_fh  700000
#define p_fok  800000
#define p_sflush  900000
#define p_rflush  1000000
// Count the number of each value of cards
std::vector<int> countCards(const std::vector<Card> &_c)
{
    std::vector<int> _numberOfCards(13, 0);
    // switch case
    for (const auto &_tc: _c) {
        _numberOfCards[_tc.getIndexes().first]++;
    }
    return _numberOfCards;
}
// Verification of Hand
std::tuple<std::string,int> newGenerateHand(const std::vector<Card> &_c){
    std::vector<int> _cc = countCards(_c);
    double _points = 0;
    if (isRFlush(_c,_cc,_points)) {
        //std::cout << "A";
        return std::make_tuple("ROYAL FLUSH !!!", _points);
    } else if (isStraightFlush(_c,_cc,_points)) {
        //std::cout << "B";
        return std::make_tuple("Straight Flush", _points);
    } else if (isFok(_cc,_points)) {
        //std::cout << "C";
        return std::make_tuple("Four Of Kind", _points);
    } else if (isFH(_cc,_points)) {
        //std::cout << "D";
        return std::make_tuple("Fullhouse", _points);
    } else if (isFlush(_c,_points)) {
        //std::cout << "E";
        return std::make_tuple("Flush", p_flush);
    } else if (isStraight(_cc,_points)) {
        //std::cout << "F";
        return std::make_tuple("Straight", _points);
    } else if (isTok(_cc,_points)) {
        //std::cout << "G";
        return std::make_tuple("Three Of Kind", _points);
    } else if (isTwoPair(_cc,_points)) {
        //std::cout << "H";
       return std::make_tuple("Two Pair", _points);
    } else if (isPair(_cc,_points)) {
        //std::cout << "I";
        return std::make_tuple("One Pair", _points);
    } else if (isNothing(_cc,_points)){
        //std::cout << "J";
        return std::make_tuple("Nothing", _points);
    } else {
        //std::cout << "K";
        return std::make_tuple("BUG", _points);
    }
}
// Verification of Cards
bool isNothing(const std::vector<int> &_cc, double &_points){
    _points = 0;
    int _cptCards = 0;
    for (int i=12;i>=0;i--){
        if (_cc[i] == 1 && _cptCards !=4) {
            _points += (i + 1) * (pow(10, 2));
            _cptCards++;
        }
        else {
            _points += _cc[i] * i;
        }
    }
    return true;
}
bool isPair(const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    bool _bestCardWithPair1 = false;
    bool _bestCardWithPair2 = false;
    bool _bestCardWithPair3 = false;
    int _onlyAPair = 0;
    bool _bestPair = false;
    for (int i=12;i>=0;i--){

        // Only Two Cards
        if (_cc[i] == 2){
            for (int i=12;i>=0;i--){
                _onlyAPair+= _cc[i];
            }
            if (_onlyAPair == 2){
                _points += p_pair + (i + 1) * (pow(10, 3));
                return true;
            }
            else {
                _onlyAPair = 0;
            }
        }

        if (!_bestCardWithPair1 && _cc[i] == 1){
            _points += (i + 1) * (pow(10, 2));
            _bestCardWithPair1 = true;
        }
        else if (!_bestCardWithPair2 && _cc[i] == 1){
            _points += (i + 1) * (pow(10, 2));
            _bestCardWithPair2 = true;
        }
        else if (!_bestCardWithPair3 && _cc[i] == 1){
            _points += (i + 1) * (pow(10, 1));
            _bestCardWithPair3 = true;
        }
        else {
            if (_cc[i]==1)
            _points += i;
        }

        if (_cc[i] == 2 && !_bestPair){
            _bestPair = true;
            _points += p_pair + (i + 1) * (pow(10, 3));
        }

    }
    if (_bestCardWithPair1 && _bestCardWithPair2 && _bestCardWithPair3 && _bestPair){
        return true;
    }

    _points=0;
    return false;
}
bool isTwoPair(const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    int _nbPair = 0;
    bool _bestCardWithTwoPair = false;
    for (int i=12;i>=0;i--){

        if ( !_bestCardWithTwoPair ){
            if ((_cc[i] >= 1 && _nbPair ==2) || _cc[i]==1){
                _points += (i + 1) * (pow(10, 2));
                _bestCardWithTwoPair = true;
            }
        }
        if (_cc[i] == 2 && _nbPair!=2 ){
            _nbPair ++;
            _points += (i + 1) * (pow(10, 3));
        }
        _points += _cc[i]*i;
    }

    if (_nbPair == 2 && _bestCardWithTwoPair){
        _points += p_2pair;
        return true;
    }
    _points = 0;
    return false;
}
bool isTok(const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    bool _bestCardWithTok1 = false;
    bool _bestCardWithTok2 = false;
    bool _finalTok = false;
    for (int i=_cc.size()-1;i>=0;i--){

        if (!_bestCardWithTok1){
            if (_cc[i] == 1){
                _points += (i + 1) * (pow(10, 2));
                _bestCardWithTok1 = true;
            }
        }
        else if (!_bestCardWithTok2){
            if (_cc[i] == 1){
                _points += (i + 1) * (pow(10, 2));
                _bestCardWithTok2 = true;
            }
        }
        if (!_finalTok){
            if (_cc[i] == 3 ){
                _points += p_tok + (i + 1) * (pow(10, 3));
                _finalTok = true;
            }
        }

        if (_cc[i] != 3){
            _points += i*_cc[i];
        }

    }
    if (_bestCardWithTok1 && _bestCardWithTok2 && _finalTok ){
        return true;
    }
    _points = 0;
    return false;

}
bool isFok(const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    bool _bestCardWithFok = false;
    bool _fok = false;

    for (int i=12;i>=0;i--){
        if (!_bestCardWithFok){
            if (_cc[i] == 1){
                _points += (i + 1) * (pow(10, 2));
                _bestCardWithFok = true;
            }
        }
        if (_cc[i] == 4 && !_fok){
            _points += p_fok + (i + 1) * (pow(10, 3));
            _fok = true;
        }
        _points += _cc[i]*i;
    }
    if (_bestCardWithFok && _fok){
        return true;
    }
    _points = 0;
    return false;
}
bool isStraight(const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    int _cptStraight = 0;
    for (int i=12;i>=0;i--){
        if (_cc[i] > 0){
            _cptStraight++;
            _points += (i+1)*std::pow(10,2);
            if (_cptStraight == 5){
                _points += p_straight;
                return true;
            }
        }
        else {
            _cptStraight = 0;
        }
    }

    // CHECK A 2 3 4 5
    if (_cc[0] > 0 && _cc[1] > 0 && _cc[2] > 0 && _cc[3] > 0 && _cc[12] > 0){
        _points = p_straight + 0 + 1 + 2 + 3 + 4;
        return true;
    }
    return false;
}
bool isFlush(const std::vector<Card> &_c,double & _points)
{
    _points = 0;
    int _col0=0; int _col1=0; int _col2=0; int _col3=0;
    for (Card _card : _c){
        if(_card.getIndexes().second==0) _col0++;
        if(_card.getIndexes().second==1) _col1++;
        if(_card.getIndexes().second==2) _col2++;
        if(_card.getIndexes().second==3) _col3++;
    }
    if (_col0 == 5 || _col1 == 5 || _col2 == 5 || _col3 == 5){
        _points = p_flush;
        return true;
    }
    _points = 0;
    return false;
}
bool isFH(const std::vector<int> &_cc,double &_points)
{
    _points = 0;
    bool _tok = false;
    bool _tokbis = false;
    bool _pair = false;
    for (int i=_cc.size()-1;i>=0;i--){
        if (_cc[i] == 3 && !_tok ){
            _tok = true;
            _tokbis = true;
            _points += p_fh + (i+1)*std::pow(10,3);
        }
        if ( (_cc[i] == 2 && !_pair )|| (_cc[i]==3 && !_tokbis)){
            _pair = true;
            _points += (i+1) * std::pow(10,2);
        }
        if (_cc[i] == 1){
            _points += i;
        }
    }
    if (_tok&&_pair){
        return true;
    }
    return false;
}
bool isStraightFlush(const std::vector<Card> &_c, const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    int _cptStraight = 0;
    std::vector<int> _indicesStraigth;

    for (int i=_cc.size()-1;i>=0;i--){

        if (_cptStraight == 5){
            int _col0=0; int _col1=0; int _col2=0; int _col3=0;
            for (Card _card : _c){
                if (_card.getIndexes().first == _indicesStraigth[0] ||
                    _card.getIndexes().first == _indicesStraigth[1] ||
                    _card.getIndexes().first == _indicesStraigth[2] ||
                    _card.getIndexes().first == _indicesStraigth[3] ||
                    _card.getIndexes().first == _indicesStraigth[4]){
                    if(_card.getIndexes().second==0) _col0++;
                    if(_card.getIndexes().second==1) _col1++;
                    if(_card.getIndexes().second==2) _col2++;
                    if(_card.getIndexes().second==3) _col3++;
                }
            }
            if (_col0 == 5 || _col1 == 5 || _col2 == 5 || _col3 == 5){
                _points += p_sflush;
            }
        }
        if (_cc[i] > 0){
            _points += i;
            _cptStraight++;
            _indicesStraigth.push_back(i);
        }
        else {
            _cptStraight = 0;
            _indicesStraigth.clear();
        }

    }

    if (_points >= p_sflush){
        return true;
    }
    return false;
}
bool isRFlush(const std::vector<Card> &_c, const std::vector<int> &_cc, double &_points)
{
    _points = 0;
    // ROYAL
    if (_cc[12]>0
    && _cc[11]>0
    && _cc[10]>0
    && _cc[9]>0
    && _cc[8]>0){
       int _col0=0; int _col1=0; int _col2=0; int _col3=0;
       // FLUSH
       for (Card _card : _c){
           if (_card.getIndexes().first >=8){
               if(_card.getIndexes().second==0) _col0++;
               if(_card.getIndexes().second==1) _col1++;
               if(_card.getIndexes().second==2) _col2++;
               if(_card.getIndexes().second==3) _col3++;
           }
       }
       if (_col0 == 5 || _col1 == 5 || _col2 == 5 || _col3 == 5){
           _points = p_rflush;
           return true;
       }
   }
    return false;
}
// Human Player Validation
bool isValidChoice(double _choice, std::vector<double> _choices){
    for (double _d : _choices){
        if (_choice == _d){
            return true;
        }
    }
    return false;
}
// Expert
int evaluateTwoCards(Card _c1,Card _c2){

    int _v1 = _c1.getIndexes().first;
    int _v2 = _c2.getIndexes().first;

    int _GoodPair = 1;
    int _BadPair = 2;
    int _GoodHand = 3;
    int _BadHand = 4;
    // 0 1 2 3 4 5 6 7 8 9 10 11 12
    // 2 3 4 5 6 7 8 9 10 J Q K A

    // Pair
    if (_v1 == _v2){
        // Good Pair 88,99..AA
        if (_v1 > 6){
            return _GoodPair;
        }
        // Bad Pair 22,33..77
        else{
            return _BadPair;
        }
    }
    // Not Pair
    else {
        // [ A + 6,7..A ] or [ K + 7,8..A ] or [ Q + 8,9..A ] or [ J + 9,10..A ] or [ 10 + ( 10,J..A ) ]
        // Good Hand
        if (_v1 + _v2 >= 16){
            return _GoodHand;
        }
        // Bad Hand
        else {
            return _BadHand;
        }
    }
}