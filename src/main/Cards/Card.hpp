/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_CARD_HPP
#define POKIA_CARD_HPP

#include <string>
#include <map>

static const std::map<std::string, int> map_value
        {{"2",  0},
         {"3",  1},
         {"4",  2},
         {"5",  3},
         {"6",  4},
         {"7",  5},
         {"8",  6},
         {"9",  7},
         {"10", 8},
         {"J",  9},
         {"Q",  10},
         {"K",  11},
         {"A",  12}};
static const std::map<std::string, int> map_color{{"^",  1},
                                                  {"<>", 2},
                                                  {"<3", 3},
                                                  {"*",  4}};

class Card {
private:
    std::string _value;
    std::string _color;
    std::pair<int, int> _indexes;
    static int valueOf(const std::string &_v);
    static int colorOf(const std::string &_c);
public:
    // Constructor
    Card(const std::string &value, const std::string &color);
    // Getters
    const std::string &getValue() const;
    const std::string &getColor() const;
    const std::pair<int, int> &getIndexes() const;
    // Methods
    void printCard();
};

#endif
