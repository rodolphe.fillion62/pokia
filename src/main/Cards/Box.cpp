/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <fstream>
#include <iostream>
#include <algorithm>
#include "Box.hpp"

// Constructor
Box::Box() {
    _size = 52;
    std::string _tempValue, _tempColor;
    std::ifstream myfile;
    myfile.open("../src/main/all_cards.txt", std::ios::out | std::ios::app | std::ios::binary);
    if (myfile.is_open()) {
        for (int i = 0; i < _size; i++) {
            myfile >> _tempValue;
            myfile >> _tempColor;
            _cards.push_back(Card(_tempValue, _tempColor));
        }
        myfile.close();
    }
    myfile.close();
}
// Getters
std::vector<Card> &Box::getCards() {
    return _cards;
}
// Methods
void Box::shuffleCards() {
    std::random_shuffle(_cards.begin(), _cards.end());
}








