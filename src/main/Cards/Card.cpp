/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <string>
#include <vector>
#include <iostream>
#include "Card.hpp"

// Constructor
Card::Card(const std::string &value, const std::string &color)
    : _value(value), _color(color), _indexes({valueOf(_value), colorOf(_color)})
{}
// Getters
const std::string &Card::getValue() const
{
  return _value;
}
const std::string &Card::getColor() const
{
  return _color;
}
int Card::valueOf(const std::string &_v)
{
  auto it = map_value.find(_v);
  return it != map_value.end() ? it->second : 0;
}
int Card::colorOf(const std::string &_c)
{
  auto it = map_color.find(_c);
  return it != map_color.end() ? it->second : 0;
}
const std::pair<int, int>& Card::getIndexes() const
{
  return _indexes;
}
// Methods
void Card::printCard()
{
  switch(colorOf(_color)){
      case 1:
          std::cout << "[" << _value << "|" << "\u2660" << "]";
          break;
      case 2:
          std::cout << "[" << _value << "|" << "\u2666" << "]";
          break;
      case 3:
          std::cout << "[" << _value << "|" << "\u2665" << "]";
          break;
      case 4:
          std::cout << "[" << _value << "|" << "\u2663" << "]";
          break;
      default:
          break;
  }
  // No utf mode
  //std::cout << "[" << _value << "|" << _color << "]";
}