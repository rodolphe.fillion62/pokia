/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_EVALFUNCTIONS_HPP
#define POKIA_EVALFUNCTIONS_HPP

// Count the number of each value of cards
std::vector<int> countCards(const std::vector<Card> &_c);
// Verification of Hand
std::tuple<std::string,int> newGenerateHand(const std::vector<Card> &_c);
// Verification of Cards
bool isNothing(const std::vector<int> &_cc, double &_points);
bool isPair(const std::vector<int> &_cc, double &_points);
bool isTwoPair(const std::vector<int> &_cc, double &_points);
bool isTok(const std::vector<int> &_cc, double &_points);
bool isFok(const std::vector<int> &_cc, double &_points);
bool isStraight(const std::vector<int> &_cc, double &_points);
bool isFlush(const std::vector<Card> &_c,double &_points);
bool isFH(const std::vector<int> &_cc,double &_points);
bool isStraightFlush(const std::vector<Card> &_c, const std::vector<int> &_cc,double &_points);
bool isRFlush(const std::vector<Card> &_c, const std::vector<int> &_cc,double &_points);
// Human
bool isValidChoice(double _choice, std::vector<double> _choices);
// Expert
int evaluateTwoCards(Card _c1,Card _c2);
#endif
