/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "Game/PowerGame.hpp"
#include <tuple>
#include <time.h>
#include <chrono>
#include <random>
#include <fstream>
static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

int main() {
    srand(time(NULL));

    std::cout << "Pokia , Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo" << std::endl;
    std::cout << "Pokia comes with ABSOLUTELY NO WARRANTY ; for details type (w)." << std::endl;
    std::cout << "This is free software, and you are welcome to redistribute it under certain conditions ; type (c) for details." << std::endl;
    std::cout << std::endl;
    std::cout << "commands: (w)(c)(game)" << std::endl;
    std::string _choice;
    std::cin >> _choice;
    while(_choice != "game"){
        if (_choice == "w"){
            std::cout << std::endl;
            std::cout << " 15. Disclaimer of Warranty." << std::endl;
            std::cout << "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW." << std::endl;
            std::cout << "EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND,"<< std::endl;
            std::cout << "EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. "<< std::endl;
            std::cout << "THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, "<< std::endl;
            std::cout << "YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION."<< std::endl;
            std::cout << std::endl;
        } else if (_choice == "c"){
            std::cout << std::endl;
            std::cout << "2. Basic Permissions." << std::endl;
            std::cout << "All rights granted under this License are granted for the term of copyright on the Program," << std::endl;
            std::cout << "and are irrevocable provided the stated conditions are met. This License explicitly affirms your unlimited permission to run the unmodified Program." << std::endl;
            std::cout << "The output from running a covered work is covered by this License only if the output," << std::endl;
            std::cout << "given its content, constitutes a covered work. This License acknowledges your rights of fair use or other equivalent," << std::endl;
            std::cout << "as provided by copyright law." << std::endl;
            std::cout << "You may make, run and propagate covered works that you do not convey, without conditions so long as your license otherwise remains in force." << std::endl;
            std::cout << "You may convey covered works to others for the sole purpose of having them make modifications exclusively for you, or provide you with facilities for running those works," << std::endl;
            std::cout << "provided that you comply with the terms of this License in conveying all material for which you do not control copyright." << std::endl;
            std::cout << "Those thus making or running the covered works for you must do so exclusively on your behalf, under your direction and control," << std::endl;
            std::cout << "on terms that prohibit them from making any copies of your copyrighted material outside their relationship with you. " << std::endl;
            std::cout << std::endl;
        } else if (_choice != "game"){
            std::cout << "wrong choice > commands: (w)(c)(game)" << std::endl;
            continue;
        }
        std::cout << "commands: (w)(c)(game)" << std::endl;
        std::cin >> _choice;
    }

    /* Game */
    Pokia();
    return 0;

    /* Game Stats */
    /*
    // sMoney = 1000 $ ; sb = 10 $ ; bb = 20 $
    std::string _fileName = "../results_Pokia/simulations_txt/IA_vs_Randomx3_300games.txt";
    std::ofstream _myfile;
    _myfile.open(_fileName,std::ios::out| std::ios::app | std::ios::binary);
    std::vector<int> _players{5,1,1,1};
    std::vector<int> _finalResult;
    int _iterationsMC = 1000; int _nbGames = 500;
    _myfile << "ExpertEnv vs Random vs Random vs Random :" << std::endl;
    _myfile << "- Nb games = " << _nbGames << std::endl;
    _myfile << "- Sb = 10$ , Bb = 20$ , Start money = 1000$" << std::endl;
    std::vector<int> _sims{_iterationsMC,0,0,0};
    _finalResult = PokiaPreset(4,_nbGames,_players,_sims);
    _myfile << "[" << _finalResult[0] << "/" << _finalResult [1] << "/" << _finalResult[2] << "/" << _finalResult[3]<< "]" << std::endl;
    _myfile.close();
    return 0;
    */
    /* Game Stats */
}

