/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include <algorithm>
#include "Game.hpp"
#include "../Players/Random_player.hpp"
#include "../Players/All_in_player.hpp"
#include "../Players/Monte_Carlo.hpp"
#include "../Players/Human_player.hpp"
#include "../Players/Expert.hpp"
#include "PowerGame.hpp"
#include "../Players/Expert_Env.hpp"
#include <chrono>
#include <random>
static std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());

// Constructors
//-principal constructor to build a game
Game::Game(int nbPlayers, double money, double sb, double bb,const std::vector<int>&_newP,const std::vector<int>& _sims) : _nbPlayers(nbPlayers), _money(money), _sb(sb),_bb(bb){
    _box.shuffleCards();
    _nMC = -1;
    for (unsigned i=0;i<_newP.size();i++){
        switch(_newP[i]){
            case 0:
                _players.push_back(new Human_player(_money));
                _players[i]->setName("Human");
                break;
            case 1:
                _players.push_back(new Random_IA(_money));
                _players[i]->setName("Random");
                break;
            case 2:
                _players.push_back(new All_in_player(_money));
                _players[i]->setName("AllIN");
                break;
            case 3:
                _players.push_back(new Monte_Carlo(_money,_sims[i]));
                _players[i]->setName("Monte Carlo");
                break;
            case 4:
                _players.push_back(new Expert(_money));
                _players[i]->setName("Expert");
                break;
            case 5:
                _players.push_back(new Expert_Env(_money));
                _players[i]->setName("Expert_Env");
                break;
        }
        _players.at(i)->setNumber(i);
    }

    _startMoney = _money;
    _highestBet = 0;
    _nbPlayerOff = 0;
    _nbPlayerFold = 0;
    _isMCExpert = false;
    _endGame = false;
    _chaineAction = "";
    _switchDealer = -1;

}
//-testing constructor
Game::Game(int nbPlayers, double money, double sb, double bb) : _nbPlayers(nbPlayers), _money(money), _sb(sb),_bb(bb) {
    _box.shuffleCards();
    _players.push_back(new Random_IA(_money));
    _players.at(0)->setNumber(0);
    _players.push_back(new Random_IA(_money));
    _players.at(1)->setNumber(1);
    _players.push_back(new Random_IA(_money));
    _players.at(2)->setNumber(2);
    _players.push_back(new Monte_Carlo(_money,30));
    _players.at(3)->setNumber(3);
    _startMoney = _money;
    _highestBet = 0;
    _nbPlayerOff = 0;
    _nbPlayerFold = 0;
    _isMCExpert = false;
    _endGame = false;
    _chaineAction = "";
    _switchDealer = -1;
    _nMC = -1;
}
//-copy constructor to simulate Monte Carlo
Game::Game(const Game& _copy, const std::vector<Card> &_envBoard,const std::vector<Card> &_envPlayer) {
    // Copy current board & player
    this->_envPlayer = _envPlayer;
    this->_envBoard = _envBoard;
    // Copy all the datas of game
    _isMCExpert = true;
    _choiceMC = 0;
    _nbPlayers = _copy._nbPlayers;
    _startMoney = _copy._money;
    _highestBet = _copy._highestBet;
    _nbPlayerOff = _copy._nbPlayerOff;
    _nbPlayerFold = _copy._nbPlayerFold;
    _endGame = _copy._endGame;
    _current = _copy._current;
    _first= _copy._first;
    _dealer = _copy._dealer;
    _nbTurn = _copy._nbTurn;
    _turn = _copy._turn;
    _money = _copy._money;
    _sb = _copy._sb;
    _bb = _copy._bb;
    _victoryFile = _copy._victoryFile;
    _nMC = _copy._nMC;
    // Copy all the players and their cards + data
    for (unsigned i=0;i<_copy._players.size();i++) {
        Player *p = new Random_IA(0);
        p->setCards(_copy._players.at(i)->getCards());
        p->setMoney(_copy._players.at(i)->getMoney());
        p->setDealer(_copy._players.at(i)->isDealer());
        p->setActive(_copy._players.at(i)->isActive());
        p->setOutOfRound(_copy._players.at(i)->isOutOfRound());
        p->setCheck(_copy._players.at(i)->isCheck());
        p->setLeader(_copy._players.at(i)->isLeader());
        p->setNumber(_copy._players.at(i)->getNumber());
        p->setFold(_copy._players.at(i)->isFold());
        p->setHb(_copy._players.at(i)->getHb());
        p->setSmallOrBigBlind(_copy._players.at(i)->getSmallOrBigBlind());
        _players.push_back(p);
    }
    // Reset cards to avoid bug during simulation
    //giveMCCards();
}
// Getters
bool Game::isEndGame() const {
    return _endGame;
}
std::vector<Player *> &Game::getPlayers() {
    return _players;
}
Board &Game::getBoard() {
    return _board;
}
double Game::getSb() const {
    return _sb;
}
double Game::getBb() const {
    return _bb;
}
double Game::getHighestBet() const {
    return _highestBet;
}
int Game::getNbPlayers() const {
    return _nbPlayers;
}
bool Game::isDisplay() const {
    return _display;
}
bool Game::isIsMcExpert() const {
    return _isMCExpert;
}
int Game::getNbWinner() const {
    return nbWinner;
}
const std::string &Game::getChaineAction() const {
    return _chaineAction;
}
// Setters
void Game::setIsMcExpert(bool isMcExpert) {
    _isMCExpert = isMcExpert;
}
void Game::setChoiceMc(double choiceMc) {
    _choiceMC = choiceMc;
}
void Game::setPlayers(std::vector<Player *> _newPlayers){
    int _cpt = 0;
    for (Player * _p : _newPlayers){
        _p->setNumber(_cpt);
        _players.push_back(_p);
        _cpt++;
    }
}
void Game::setNMc(int nMc) {
    _nMC = nMc;
}
void Game::setSwitchDealer(int switchDealer) {
    _switchDealer = switchDealer;
}
// Display
void Game::printStartGame() {
    std::cout << "* ------------------------------ *" << std::endl;
    std::cout << "[        POKIA running ...       ]" << std::endl;
    std::cout << "* ------------------------------ *" << std::endl;
    std::cout << "|  Number of players : " << _nbPlayers << "         |" << std::endl;
    std::cout << "|  Money given to players : " << _startMoney << "$ |" << std::endl;
    std::cout << "|  Small blind : " << _sb << "$             |" << std::endl;
    std::cout << "|  Big blind : " << _bb << "$               |" << std::endl;
    std::cout << "* ------------------------------ *" << std::endl;
    std::cout << std::endl;
}
void Game::printPlayers() {
    std::cout << "* ---------- PLAYERS ---------- *" << std::endl;
    for (Player *_p : _players) {
        std::cout << "Player{" << _p->getNumber() << "} ";
        _p->printCards();
        std::cout << " Money: " << _p->getMoney() << "$";
        std::cout << std::endl;
    }
    std::cout << "* ------------------------------ *" << std::endl;
    std::cout << std::endl;
}
void Game::printAction(std::string _choice,int _b) {
    std::cout  << "[" << _choice << "] " << _b << " $" << std::endl;
}
void Game::jumpText() {
    std::cout << std::endl;
}
// Methods
bool Game::checkHB() {
    if (_highestBet == 0) return false;
    for (Player *_p : _players) {
        if (!_p->isOutOfRound()) {
            if (_p->getHb() != _highestBet) {
                return false;
            }
        }
    }
    return true;
}
bool Game::checkCHECK() {
    for (Player *_p : _players) {
        if (_p->isActive()) {
            if (!_p->isCheck()) {
                return false;
            }
        }
    }
    return true;
}
bool Game::onlyOneActiveLeft() {
    int _cpt=0;
    for(Player *p : _players){
        if (p->isActive()){
            _cpt++;
        }
    }
    return _cpt==1;
}
void Game::checkMoneyOfPlayers() {
    for (unsigned i = 0; i < _players.size(); i++) {
        if (_players.at(i)->getMoney() == 0) {
            if (!_isMCExpert  && _display) {
                std::cout << "Player(" << _players.at(i)->getNumber() << ") lost !" << std::endl;
            }
            _players.erase(_players.begin() + i);
            _nbPlayers--;
        }
    }
    if (_nbPlayers==1){
        nbWinner = _players.at(0)->getNumber();
    }
}
// Game
bool Game::run() {
    if (_nbPlayers < 2) {
        std::cout << "Not enough players, please put at least 2.";
        return false;
    } else if (_nbPlayers > 8) {
        std::cout << "Too much players, please put less than 8.";
        return false;
    } else if (_nbPlayers <= 8) {
        return true;
    }
    return false;
}
void Game::start(bool _d) {
    // Set display to see the entire game
    this->_display = _d;
    if (run()) {
        int _rBB,_rSB;
        if (!_isMCExpert){
            _nbTurn = 0;
            _first = 0;
            // Made to switch dealer in n games
            if (_switchDealer ==-1){
                _dealer = 0;
            }else {
                _dealer = _switchDealer;
            }
        }

        if (!_isMCExpert && _display)
            printStartGame();

        // Loop of the entire Game
        while(true) {
            // End Of Game
            if (_players.size() == 1) {
                _endGame = true;
                break;
            }
            // Display Start of round
            if (!_isMCExpert && _display) {
                std::cout << std::endl;
                std::cout << "* * Début du tour n " << _nbTurn << " * *" << std::endl;
            }
            // Set Dealer & SB & BB
            if (!_isMCExpert) {
                if (_players.size() == 2) {
                    for (Player *_p : _players){
                        _p->setDealer(false);
                    }
                    _first = _dealer;
                    // Big blind
                    if (_bb > _players.at(getCurrentPlay(_dealer + 1))->getMoney()){
                        _players.at(getCurrentPlay(_dealer + 1))->setHb(_players.at(getCurrentPlay(_dealer + 1))->getMoney());
                    }
                    else {
                        _players.at(getCurrentPlay(_dealer + 1))->setHb(_bb);
                    }
                    // Small blind
                    if (_sb > _players.at(getCurrentPlay(_dealer ))->getMoney()){
                        _players.at(getCurrentPlay(_dealer ))->setHb(_players.at(getCurrentPlay(_dealer ))->getMoney());
                    }
                    else {
                        _players.at(getCurrentPlay(_dealer ))->setHb(_sb);
                    }
                    // Dealer
                    _players.at(getCurrentPlay(_dealer))->setDealer(true);
                    _players.at(getCurrentPlay(_dealer+1))->setDealer(false);
                }
                else if(_players.size()==3){
                    for (Player *_p : _players){
                        _p->setDealer(false);
                    }
                    // dealer = 0 or definied in main.cpp -> speak before SB
                    _players.at(getCurrentPlay(_dealer))->setDealer(true);
                    // SB : dealer +1 -> speak before BB
                    _rSB = _dealer +1;
                    _players.at(getCurrentPlay(_rSB))->setHb(_sb);
                    // BB : dealer +2 -> speak LAST
                    _rBB = _dealer + 2;
                    _players.at(getCurrentPlay(_rBB))->setHb(_bb);
                }
                else if (_players.size() > 3) {
                    // Reset Dealer
                    for (Player *_p : _players){
                        _p->setDealer(false);
                    }
                    // Order : P3(0) P4(0) P0(d) P1(sb) P2(bb) PREFLOP
                    // Order : P1(SB) P2(BB) P3(0) P4(0) P0(d) POSTFLOP
                    // sb = dealer +1
                    // bb = dealer +2
                    // first = bb + 1
                    // dealer = 0 or definied in main.cpp -> speak before SB
                    _players.at(getCurrentPlay(_dealer))->setDealer(true);
                    // SB : dealer +1 -> speak before BB
                    _rSB = _dealer +1;
                    _players.at(getCurrentPlay(_rSB))->setHb(_sb);
                    // BB : dealer +2 -> speak LAST
                    _rBB = _dealer + 2;
                    _players.at(getCurrentPlay(_rBB))->setHb(_bb);
                }
                _highestBet = 0;


            }

            // Simulation called with Monte_Carlo.cpp & Mcts.cpp
            if(_isMCExpert){
                bool _GOMC =true;
                betRoundMC(_current,_nMC,_GOMC);
                winRound();
                return;
            }
            // Normal round during the game
            else {
                bool _GOMC =false;
                giveCards();
                betRoundMC(_current,_nMC,_GOMC);
                winRound();
                checkMoneyOfPlayers();
            }

            _nbTurn++;
            resetRound();
            _dealer++;

        }
    }
}
// Round
void Game::betRoundMC(int _c,int _nMC,bool &_mcChoice) {
    int _turnMc = 0;
    if (_mcChoice){
        if (_envBoard.size() == 0){
            _turnMc = 0;
        }
        else {
            _turnMc= _envBoard.size() - 2;
        }
    }
    else {
        _turnMc = 0;
    }
    _board.setMoney(0);
    for (int i=_turnMc;i<4;i++){
        if (!_mcChoice)_turn = 0;
        if (!_mcChoice) _current = _c;
        std::string _action = "";
        bool _endRound = false;
        if (!_mcChoice)leaderRound(_players.at(getCurrentPlay(_current))->getNumber());
        // 4 Turns
        if (!_mcChoice) {
            switch (i) {
                case 0:
                    _highestBet = _bb;
                    break;
                case 1:
                    _highestBet = 0;
                    Flop();
                    break;
                case 2:
                    _highestBet = 0;
                    Turn();
                    break;
                case 3:
                    _highestBet = 0;
                    River();
                    break;
                default:
                    break;
            }
        }
        while (true) {
            // Find Dealer
            if (!_mcChoice){
                for (Player *_p : _players){
                    if (_p->isDealer()){
                        if (i==0){
                            if (_players.size()==2){
                                _current = _p->getNumber();
                            }
                            else {
                                _current = _p->getNumber() +3;
                            }


                        } else {
                            if (_players.size()==2){
                                _current = _p->getNumber();
                            }
                            else {
                                _current = _p->getNumber() +1;
                            }
                        }
                    }
                }
            }
            // End Round Verification
            if (!_mcChoice) {
                if (checkCHECK() || checkHB() || onlyOneActiveLeft() ||
                    (_nbPlayerOff + _nbPlayerFold == _nbPlayers - 1) || (_nbPlayerOff + _nbPlayerFold == _nbPlayers)) {
                    _endRound = true;
                }
            }
            // End of this round
            if (_endRound) {
                // Set all the datas to players
                for (Player *_p: _players) {
                    _p->pay(std::min(_p->getHb(),_highestBet), *this);
                    _p->setStackRound(_p->getStackRound() + std::min(_p->getHb(),_highestBet));
                    _p->setHb(0);
                    _p->setLeader(false);
                    _p->setCheck(false);
                }
                break;
            }
            while (true){

                // While we are not on leader & different informations are not verified
                if (( _players.at(getCurrentPlay(_current))->isLeader() && _turn > 0) && !_mcChoice){
                    if (checkHB()){
                        _endRound=true;
                        break;
                    }
                    if (onlyOneActiveLeft()){
                        _endRound=true;
                        break;
                    }
                    if ( checkCHECK()){
                        _endRound=true;
                        break;
                    }
                    if ((_nbPlayerOff+_nbPlayerFold == _nbPlayers - 1 ) || (_nbPlayerOff + _nbPlayerFold == _nbPlayers)){
                        _endRound=true;
                        break;
                    }
                }

                // Lowest stack calculation
                if ( _players.at(getCurrentPlay(_current))->isActive() && !_mcChoice) {
                    double _lowestStack = 9999;
                    for (Player *p : _players) {
                        if (!p->isOutOfRound()) {
                            if ((p->getMoney()) < _lowestStack) {
                                _lowestStack = (p->getMoney());
                            }
                        }
                    }
                    if (_display && !_isMCExpert) std::cout << "[hbb=" << _highestBet << "] [cbp=" << _players.at(getCurrentPlay(_current))->getHb() << "] [lsp=" << _lowestStack << "]" << std::endl;
                }

                // Set leader
                if (_turn == 0 && !_mcChoice) {
                    leaderRound(_players.at(getCurrentPlay(_current))->getNumber());
                }

                // Only active player speak
                if (_players.at(getCurrentPlay(_current))->isActive()) {

                    // Verify Fold & Check
                    if (!_mcChoice && (onlyOneActiveLeft() && (_nbPlayerFold  == _nbPlayers - 1))) {
                        _endRound = true;
                        break;
                    }
                    if (!_mcChoice && (onlyOneActiveLeft() && ( _players.at(getCurrentPlay(_current))->isCheck()))) {
                        _endRound = true;
                        break;
                    }

                    double _actualBet = 0;

                    // Monte Carlo Bet
                    if (_mcChoice && _isMCExpert) {
                        if (_players.at(getCurrentPlay(_current))->getNumber() == _nMC) {
                            _actualBet = _choiceMC;
                            _mcChoice = false;
                        } else {
                            _actualBet = _players.at(getCurrentPlay(_current))->speak(*this);
                        }
                    }
                        // Normal Bet
                    else {
                        _actualBet = _players.at(getCurrentPlay(_current))->speak(*this);
                    }

                    // CHECK / FOLD
                    if (_actualBet == 0) {
                        // CHECK
                        if (_players.at(getCurrentPlay(_current))->getHb() == _highestBet) {
                            _players.at(getCurrentPlay(_current))->setCheck(true);
                            _action = "CHECK";
                        }
                            // NO MORE MONEY
                        else if (_players.at(getCurrentPlay(_current))->getMoney() - _players.at(getCurrentPlay(_current))->getHb() == 0) {
                            _players.at(getCurrentPlay(_current))->setActive(false);
                            _nbPlayerOff++;
                            _action = "ALL IN no money";
                        }
                            // FOLD
                        else {
                            _players.at(getCurrentPlay(_current))->setActive(false);
                            _players.at(getCurrentPlay(_current))->setOutOfRound(true);
                            _nbPlayerFold++;
                            _action = "FOLD";
                        }
                    }
                        //CALL
                    else if (_actualBet == _highestBet || _actualBet == _highestBet - _players.at(getCurrentPlay(_current))->getSmallOrBigBlind()) {
                        // CHECK
                        if (_players.at(getCurrentPlay(_current))->getHb() == _highestBet) {
                            _players.at(getCurrentPlay(_current))->setCheck(true);
                            _action = "CHECK";
                        } // CALL
                        else if (onlyOneActiveLeft() && _actualBet == _highestBet) {
                            _action = "CALL";
                            _players.at(getCurrentPlay(_current))->setActive(false);
                            _nbPlayerOff++;
                        } else {
                            _action = "CALL";
                        }
                    }
                        // RAISE
                    else if (_actualBet > _highestBet) {
                        _highestBet = _actualBet;
                        leaderRound(_players.at(getCurrentPlay(_current))->getNumber());
                        _action = "RAISE";
                    }
                    // ALL IN
                    if (_actualBet == _players.at(getCurrentPlay(_current))->getMoney()) {
                        _players.at(getCurrentPlay(_current))->setActive(false);
                        _nbPlayerOff++;
                        _action = "ALL IN";
                    }

                    // Set maximum bet of the player
                    if (_actualBet > _players.at(getCurrentPlay(_current))->getHb()) {
                        _players.at(getCurrentPlay(_current))->setHb(_actualBet);
                    } else {
                        _players.at(getCurrentPlay(_current))->setHb( _players.at(getCurrentPlay(_current))->getHb());
                    }

                    // Display action
                    if (_display & !_isMCExpert)printAction(_action, _actualBet);

                    // Save history
                    _players.at(getCurrentPlay(_current))->setChainAction(_players.at(getCurrentPlay(_current))->getChainAction()+"["+_action+"="+std::to_string((int)_actualBet)+"]");
                    _chaineAction += "i=" + std::to_string(i) + " P["+std::to_string(_players.at(getCurrentPlay(_current))->getNumber())+"]: "+"["+_action+"="+std::to_string((int)_actualBet)+"] ";

                    // End of turn
                    if (_display &!_isMCExpert) jumpText();

                }
                _current++;
                _turn++;

            }// end leader
        }// end true
    }//run
}
void Game::winRound() {

    // Init params
    std::string _resultS = "";
    int _maxResult = 0;
    int _numPlayer=-1;
    std::tuple<std::string,double> _hE;
    // Search best player
    for (Player *_p: _players) {
        if (!_p->isOutOfRound()){
            _hE = newGenerateHand(_p->getCards());
            _p->setPoints(std::get<1>(_hE));
            if (_maxResult < std::get<1>(_hE)) {
                _maxResult = std::get<1>(_hE);
                _numPlayer = _p->getNumber();
                _resultS = std::get<0>(_hE);
            }
        }
    }

    // In case of DRAW
    std::vector<int> _winners;
    for (Player *_p: _players) {
        if (!_p->isOutOfRound()) {
            if (_p->getPoints() == _maxResult) {
                _winners.push_back((_p->getNumber()));
            }
        }
    }

    // Save history of the game
    _chaineAction += " ->WINNER[" + std::to_string(_numPlayer )+ "] with" + _resultS + "> score[" + std::to_string(_maxResult) + "]" ;

    // Display win
    if(!_isMCExpert && _display ){
        for (unsigned i=0;i<_winners.size();i++){
            std::cout << "----------------------------------" << std::endl;
            std::cout << "Winner: Player["<< _winners[i] <<"] with <" << _resultS << "> score[" << _maxResult << "] an amount of " << _board.getMoney() <<std::endl;
        }
    }
    else {
        // Uncomment to see all the win of Monte Carlo simulation
        /*
        std::cout << "MC SIMULATIONS-> " ;
        std::cout << "WIN:"<< _numPlayer<<" with <" << _resultS << "> score[" << _maxResult << "]" <<std::endl;
        */
    }

    // Set money of the players & board
    for (Player *_p: _players) {
        if (!_p->isOutOfRound()) {
            for (unsigned i = 0; i < _winners.size(); i++) {
                if (_p->getNumber() == _winners[i]) {
                    _p->setMoney(_p->getMoney() + (int) (_board.getMoney() / _winners.size()));

                }
            }
        }
    }
    _board.setMoney(0);


    // Display players
    if (!_isMCExpert && _display) {
        printPlayers();
    }
}
void Game::resetRound() {
    _box = Box();
    _box.shuffleCards();
    _board.getCards().clear();
    _board.setMoney(0);
    _nbPlayerFold=0;
    _nbPlayerOff=0;
    //_dealer = 0;
    for (Player *_p: _players) {
        _p->setDealer(false);
        _p->setOutOfRound(false);
        _p->setCheck(false);
        _p->setFold(false);
        _p->setSmallOrBigBlind(0);
        _p->getCards().clear();
        _p->setActive(true);
        _p->setHb(0);
    }
}
void Game::Flop() {
    for (Player *_p : _players) {
        _p->getCards().push_back(_board.getCards()[0]);
        _p->getCards().push_back(_board.getCards()[1]);
        _p->getCards().push_back(_board.getCards()[2]);
        if (_p->getName()=="Expert_Env"){
            _p->setScoreHands(simulationWithVec(200, _p->getCards()));
        }
    }

    if (!_isMCExpert && _display)
        _board.printFlop();
}
void Game::Turn() {
    for (Player *_p : _players) {
        _p->getCards().push_back(_board.getCards()[3]);
        if (_p->getName()=="Expert_Env"){
            _p->setScoreHands(simulationWithVec(200, _p->getCards()));
        }
    }
    if (!_isMCExpert && _display)
        _board.printTurn();
}
void Game::River() {
    for (Player *_p : _players) {
        _p->getCards().push_back(_board.getCards()[4]);
        if (_p->getName()=="Expert_Env"){
            _p->setScoreHands(simulationWithVec(200, _p->getCards()));
        }
    }
    if (!_isMCExpert && _display)
        _board.printRiver();
}
// Players
int Game::getCurrentPlay(int _i) {
    if (unsigned (_i) < _players.size()) {
        return _i;
    }
    return (_i % _players.size());
}
void Game::leaderRound(int _number) {
    for (Player *_p: _players) {
        if (_p->getNumber() == _number) {
            _p->setLeader(true);
        } else {
            _p->setLeader(false);
        }
    }
}
int Game::getPlayerWithNumber(int _number){
    int _cpt = 0;
    for (Player *_p:_players){
        if (_p->getNumber() == _number){
            return _cpt;
        }
        _cpt++;
    }
    return _cpt;
}
// Cards
void Game::giveMCCards(){

    _box = Box();
    _box.shuffleCards();

    std::vector<std::string> _values {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    std::vector<std::string> _colors {"^","<>","<3","*"};

    int _sizeCards = _players.at(getPlayerWithNumber(_nMC))->getCards().size();
    
    // Clear Player cards
    std::vector<Card> _tempCard;
    for (Player *_p : _players){
        _p->setCards(_tempCard);
    }

    // Set 2 Cards of MC and Set RANDOM Board with the initial size of card vector
    _players.at(getPlayerWithNumber(_nMC))->getCards().push_back(_envPlayer[0]);
    _players.at(getPlayerWithNumber(_nMC))->getCards().push_back(_envPlayer[1]);

    // Delete 2 Mc Cards of the box
    for (unsigned i = 0; i < _box.getCards().size(); i++) {
        // Envplayer 0
        if (_box.getCards()[i].getValue() == _envPlayer[0].getValue() && _box.getCards()[i].getColor() == _envPlayer[0].getColor()) {
            _box.getCards().erase(_box.getCards().begin() + i);
        }
        // Envplayer 1
        if (_box.getCards()[i].getValue() == _envPlayer[1].getValue() && _box.getCards()[i].getColor() == _envPlayer[1].getColor()) {
            _box.getCards().erase(_box.getCards().begin() + i);
        }
    }

    // Clear Board cards
    _board.getCards().clear();

    // Set Envboard to board
    for (unsigned i = 0; i < _envBoard.size(); i++) {
        _board.getCards().push_back(_envBoard[i]);
        // Delete Envboard card
        for (unsigned k = 0; k < _box.getCards().size(); k++) {
            if (_box.getCards()[k].getValue() == _envBoard[i].getValue() && _box.getCards()[k].getColor() == _envBoard[i].getColor()) {
                _box.getCards().erase(_box.getCards().begin() + k);
            }
        }
    }

    // Fill board if its size isnt 5
    for (int i=_envBoard.size();i<5;i++){
        _board.getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();
    }

    // Fill cards of the Monte Carlo player with envboard
    int _cptEnv = 0;
    for (int i=2;i<_sizeCards;i++){
        _players.at(getPlayerWithNumber(_nMC))->getCards().push_back(_envBoard[_cptEnv]);
        _cptEnv++;
    }

    // Fill cards of the other players
    for (Player *_p : _players){
        if (_p->getNumber()!=_nMC){
            _p->getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
            _box.getCards().pop_back();
            _p->getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
            _box.getCards().pop_back();
            for (int i=2;i<_sizeCards;i++){
                _p->getCards().push_back(_envBoard[i-2]);
            }
        }
    }
}
void Game::giveCards() {

    // Players
    std::vector<std::string> _values {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    std::vector<std::string> _colors {"^","<>","<3","*"};

    Card _A1(_values[12],_colors[0]);
    Card _A2(_values[12],_colors[1]);

    // Uncomment to set specific cards to player
    /*
        // Delete 2 Mc Cards of the box
        for (unsigned i = 0; i < _box.getCards().size(); i++) {
            // ENVPLAYER 0
            if (_box.getCards()[i].getValue() == _A1.getValue() && _box.getCards()[i].getColor() == _A1.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
            // ENVPLAYER 1
            if (_box.getCards()[i].getValue() == _A2.getValue() && _box.getCards()[i].getColor() == _A2.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
        }
    */

    Card _A3(_values[12],_colors[2]);
    Card _A4(_values[12],_colors[3]);

    // Uncomment to set specific cards to player
    /*
        // Delete 2 Mc Cards of the box
        for (unsigned i = 0; i < _box.getCards().size(); i++) {
            // ENVPLAYER 0
            if (_box.getCards()[i].getValue() == _A3.getValue() && _box.getCards()[i].getColor() == _A3.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
            // ENVPLAYER 1
            if (_box.getCards()[i].getValue() == _A4.getValue() && _box.getCards()[i].getColor() == _A4.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
        }
    */

    // Players
    for (Player *_p : _players) {
        // Uncomment to set cards to player
        /*
        if (_p->getNumber() == 0){
            _p->getCards().push_back(_A1);
            _p->getCards().push_back(_A2);
        }
        else {*/
            _p->getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
            _box.getCards().pop_back();
            _p->getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
            _box.getCards().pop_back();
            if (_p->getName()=="Expert"){
                _p->setScoreHands(simulationWith(1000, _p->getCards()[0], _p->getCards()[1]));
            }

        //}
    }

    // Board
    for (int i = 0; i < 5; i++) {
        _board.getCards().push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();
    }

}
// Destructor
Game::~Game() {
    for (auto p: _players) {
        delete p;
    }
}











