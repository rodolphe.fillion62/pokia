/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_POWERGAME_HPP
#define POKIA_POWERGAME_HPP

#include <iostream>
#include <algorithm>
#include <vector>
#include "Game.hpp"
#include "../Players/Random_player.hpp"
#include "../Players/All_in_player.hpp"
#include "../Players/Monte_Carlo.hpp"
#include <chrono>
#include <random>

// Engine - Selection of Games
std::vector<int> Pokia();
std::vector<int> PokiaPreset(int nbPlayers,int nbOfGames,std::vector<int> players,std::vector<int> simsMC);
// Simulation of Winning Hands
double simulationWith(int _nbTimes,Card _c1,Card _c2);
double simulationWithVec(int _nbTimes,std::vector<Card> _cards);
void generateWRtxt();
double searchInTxt(Card _c1,Card _c2);

#endif
