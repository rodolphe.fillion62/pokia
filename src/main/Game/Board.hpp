/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_BOARD_HPP
#define POKIA_BOARD_HPP

#include <vector>
#include "../Cards/Card.hpp"

class Board {
private:
    double _money;
    std::vector<Card> _cards;
public:
    // Constructor
    Board();
    // Getters
    double getMoney();
    std::vector<Card> &getCards();
    // Setters
    void setMoney(double money);
    void setCards(const std::vector<Card> &cards);
    // Methods
    void printFlop();
    void printTurn();
    void printRiver();
    void printBoard();
};

#endif