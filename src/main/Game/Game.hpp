/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef POKIA_GAME_HPP
#define POKIA_GAME_HPP

#include "../Cards/Box.hpp"
#include "Board.hpp"
#include "../Cards/EvalFunctions.hpp"
#include <tuple>
#include <vector>

class Player;

class Game {
private:
public:
private:
    int _nbPlayers,_nbPlayerOff,_nbPlayerFold,_victoryFile,_nMC,nbWinner;
    double _money, _sb, _bb, _highestBet,_startMoney,_choiceMC;
    bool _endGame,_isMCExpert,_display;
    int _current,_first,_dealer,_nbTurn,_turn,_switchDealer;
    Box _box;
    Board _board;
    std::vector<Player*> _players;
    std::vector<Card> _envBoard;
    std::vector<Card> _envPlayer;
    std::string _chaineAction;
public:
    // Constructors
    //-principal constructor to build a game
    Game(int nbPlayers, double money, double sb, double bb,const std::vector<int> &_newP,const std::vector<int>& _sims);
    //-testing constructor
    Game(int nbPlayers, double money, double sb, double bb);
    //-copy constructor to simulate Monte Carlo
    Game(const Game& _copy,const std::vector<Card>& _envBoard,const std::vector<Card> &_envPlayer);
    // Getters
    Board &getBoard();
    double getSb() const;
    double getBb() const;
    double getHighestBet() const;
    int getNbPlayers() const;
    int getNbWinner() const;
    const std::string &getChaineAction() const;
    std::vector<Player *> &getPlayers();
    bool isIsMcExpert() const;
    bool isEndGame() const;
    bool isDisplay() const;
    // Setters
    void setNMc(int nMc);
    void setChoiceMc(double choiceMc);
    void setIsMcExpert(bool isMcExpert);
    void setPlayers(std::vector<Player *> _newPlayers);
    void setSwitchDealer(int switchDealer);
    // Display
    void printStartGame();
    void printPlayers();
    void printAction(std::string _choice, int _b);
    void jumpText();
    // Methods
    bool checkHB();
    bool checkCHECK();
    bool onlyOneActiveLeft();
    void checkMoneyOfPlayers();
    // Game
    bool run();
    void start(bool _d);
    void winRound();
    // Players
    int getCurrentPlay(int _i);
    void leaderRound(int _number);
    int getPlayerWithNumber(int _number);
    // Cards
    void giveCards();
    void giveMCCards();
    // Round
    void resetRound();
    void betRoundMC(int _c,int _nMC,bool &_mcChoice);
    void Flop();
    void Turn();
    void River();
    // Destructor
    ~Game();

};

#endif
