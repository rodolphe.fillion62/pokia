/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <fstream>
#include "PowerGame.hpp"

// Engine - Selection of Games
std::vector<int> Pokia(){

    int _nbPlayers,_nbOfGames,_nbSimulations,_choicePlayer;
    double _sb,_bb,_startMoney;
    std::string _display;
    std::vector<int> _wins;
    //int dealer = 0;

    // Params of the game
    std::cout << "Welcome to Pokia !" << std::endl;
    std::cout << "Please follow the instructions to start the game :" << std::endl;
    std::cout << "- Number of players [2-8] :" << std::endl;
    std::cin >> _nbPlayers;
    std::cout << "- Start Money :" << std::endl;
    std::cin >> _startMoney;
    std::cout << "- Small Blind:" << std::endl;
    std::cin >> _sb;
    std::cout << "- Big Blind:" << std::endl;
    std::cin >> _bb;
    std::cout << "Number of GAMES :" << std::endl;
    std::cin >> _nbOfGames;

    // PRESET
    /*
    _nbPlayers = 4;
    _startMoney = 1000;
    _sb = 10;
    _bb = 20;
    _nbOfGames = 100;
     */

    // Make the game
    Game _pokia(_nbPlayers,_startMoney,_sb,_bb);

    // Choose the players
    std::vector<std::string> _labelPlayers = { "Human","Random","AllIn","MonteCarlo","Expert","Expert_Env"};
    std::vector<int> _newPlayers;
    std::vector<int> _simsMC;
    // Create all the players
    for (int i=0;i<_nbPlayers;i++){
        std::cout << "Player Selection [0=Human] [1=Random] [2=AllIN] [3=MonteCarlo] [4=Expert] [5=Expert_Env]" << std::endl;
        _wins.push_back(0);
        _simsMC.push_back(0);
        std::cin >> _choicePlayer;
        _newPlayers.push_back(_choicePlayer);
        if (_choicePlayer == 3 ){
            std::cout << "- Number Monte Carlo Simulations :";
            std::cin >> _nbSimulations;
            _simsMC[i] = _nbSimulations;
        }
        std::cout << _labelPlayers[_choicePlayer] << " selected !" << std::endl;
    }

    // Display of the game
    std::cout << "- Display of the game [y/n] :" << std::endl;
    std::cin >> _display;

    int dealer = 0;
    // Loop for all the simulations
    for (int i=0;i<_nbOfGames;i++){
        std::cout << "Simulation ["<< i << "/" << _nbOfGames <<  "] ..." << std::endl;
        Game _pokia(_nbPlayers,_startMoney,_sb,_bb,_newPlayers,_simsMC);

        // Change the dealer if _nbOfGames > 1
        if (dealer > _nbPlayers){
            dealer = 0;
        }
        _pokia.setSwitchDealer(dealer);
        dealer++;

        // Display Game
        if (_display == "y"){
            _pokia.start(true);
        }
        else if (_display == "n"){
            _pokia.start(false);
        }

        // Save results
        _wins[_pokia.getPlayers()[0]->getNumber()]++;
    }

    for(unsigned i=0;i<_wins.size();i++){
        std::cout << _wins[i] << " ";
    }
    std::cout << std::endl;

    return _wins;

}
std::vector<int> PokiaPreset(int nbPlayers,int nbOfGames,std::vector<int> players,std::vector<int> simsMC){

    int _nbPlayers,_nbOfGames;
    double _sb,_bb,_startMoney;
    std::vector<int> _wins;
    int dealer = 0;

    // PRESET
    _startMoney = 1000;
    _sb = 10;
    _bb = 20;
    _nbPlayers = nbPlayers;
    _nbOfGames = nbOfGames;

    // Player Selection [0=Human] [1=Random] [2=AllIN] [3=MonteCarlo] [4=Expert]

    // Make the game
    Game _pokia(_nbPlayers,_startMoney,_sb ,_bb);

    // Create all the players
    for (int i=0;i<_nbPlayers;i++){
        _wins.push_back(0);
    }

    // Loop for all the simulations
    for (int i=0;i<_nbOfGames;i++){
        std::cout << "Simulation ["<< i << "/" << _nbOfGames <<  "] ..." << std::endl;
        Game _pokia(_nbPlayers,_startMoney,_sb,_bb,players,simsMC);

        // Change the dealer if _nbOfGames > 1
        if (dealer > _nbPlayers){
            dealer = 0;
        }
        _pokia.setSwitchDealer(dealer);
        dealer++;

        _pokia.start(false);

        // Save results
        _wins[_pokia.getPlayers()[0]->getNumber()]++;
    }

    for(unsigned i=0;i<_wins.size();i++){
        std::cout << _wins[i] << " ";
    }
    std::cout << std::endl;

    return _wins;

}
// Simulation of Winning Hands
double simulationWith(int _nbTimes,Card _c1,Card _c2){

    double _nbWins = 0;
    for (int i=0;i<_nbTimes;i++) {

        Box _box;
        _box.shuffleCards();

        // Init first player with params
        std::vector<Card> _p1 = {_c1, _c2};

        // Delete 2 Cards of the box
        for (unsigned i = 0; i < _box.getCards().size(); i++) {
            // c1
            if (_box.getCards()[i].getValue() == _c1.getValue() && _box.getCards()[i].getColor() == _c1.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
            // c2
            if (_box.getCards()[i].getValue() == _c2.getValue() && _box.getCards()[i].getColor() == _c2.getColor()) {
                _box.getCards().erase(_box.getCards().begin() + i);
            }
        }

        // Init second player with random
        std::vector<Card> _p2;
        _p2.push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();
        _p2.push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();

        // Fill hand of the two players with a random board
        for (int k = 0; k < 5; k++) {
            Card _b = _box.getCards()[_box.getCards().size() - 1];
            _box.getCards().pop_back();
            _p1.push_back(_b);
            _p2.push_back(_b);
        }

        // Inc if first player has winning hand
        if (std::get<1>(newGenerateHand(_p1)) >= std::get<1>(newGenerateHand(_p2))) {
            _nbWins++;
        }

    }

    return (double)(_nbWins/_nbTimes);
}
double simulationWithVec(int _nbTimes,std::vector<Card> _cards){

    double _nbWins = 0;
    for (int i=0;i<_nbTimes;i++) {

        Box _box;
        _box.shuffleCards();

        // Init first player with params
        std::vector<Card> _p1 = _cards;

        // Delete x Cards of the box
        for (Card _c : _p1){
            for (unsigned i = 0; i < _box.getCards().size(); i++) {
                if (_box.getCards()[i].getValue() == _c.getValue() && _box.getCards()[i].getColor() == _c.getColor()) {
                    _box.getCards().erase(_box.getCards().begin() + i);
                }
            }
        }

        // Init second player with random
        std::vector<Card> _p2;
        _p2.push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();
        _p2.push_back(_box.getCards()[_box.getCards().size() - 1]);
        _box.getCards().pop_back();

        for (unsigned i=2;i<_p1.size();i++){
            _p2.push_back(_p1[i]);
        }

        // Fill hand of the two players with a random board
        for (int k = _p1.size()-2; k < 5; k++) {
            Card _b = _box.getCards()[_box.getCards().size() - 1];
            _box.getCards().pop_back();
            _p1.push_back(_b);
            _p2.push_back(_b);
        }

        // Inc if first player has winning hand
        if (std::get<1>(newGenerateHand(_p1)) >= std::get<1>(newGenerateHand(_p2))) {
            _nbWins++;
        }

    }

    return (double)(_nbWins/_nbTimes);
}
void generateWRtxt(){
    std::string _fileName = "../src/main/all_wr.txt";
    std::ofstream _myfile;
    _myfile.open(_fileName,std::ios::in| std::ios::app | std::ios::binary);

    std::string v1,v2,c1,c2;

    std::vector<std::string> _values {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    std::vector<std::string> _colors {"^","<>","<3","*"};
    Box _b;
    int cpt = 0;
    while (!_b.getCards().empty()){
        std::cout << cpt << std::endl;
        cpt++;
        // Current Card
        Card _currentC = _b.getCards()[_b.getCards().size() - 1];
        _b.getCards().pop_back();

        for (Card _c : _b.getCards()){

            _myfile << _currentC.getValue() << " " << _currentC.getColor()  << " " << _c.getValue() << " " << _c.getColor() <<  " " << simulationWith(1000,Card(_currentC.getValue(),_currentC.getColor()),Card(_c.getValue(),_c.getColor())) << std::endl;
        }
    }
}
double searchInTxt(Card _c1,Card _c2){
    std::string _fileName = "../src/main/all_wr.txt";
    std::ifstream _myfile;
    _myfile.open(_fileName,std::ios::out| std::ios::app | std::ios::binary);
    std::string _tv1,_tc1,_tv2,_tc2;
    double _score;
    while(true){
        _myfile >> _tv1;
        _myfile >> _tc1;
        _myfile >> _tv2;
        _myfile >> _tc2;
        _myfile >> _score;
        if ((_tv1 == _c1.getValue() && _tc1 == _c1.getColor() && _tv2 == _c2.getValue() && _tc2 == _c2.getColor())
            || ( _tv2 == _c1.getValue() && _tc2 == _c1.getColor() && _tv1 == _c2.getValue() && _tc1 == _c2.getColor())){
            return _score;
        }
    }
}