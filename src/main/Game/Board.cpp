/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <iostream>
#include "Board.hpp"

// Constructor
Board::Board() {}
// Getters
double Board::getMoney() {
    return _money;
}
std::vector<Card> &Board::getCards() {
    return _cards;
}
// Setters
void Board::setMoney(double money) {
    _money = money;
}
void Board::setCards(const std::vector<Card> &cards) {
    _cards = cards;
}
// Methods
void Board::printFlop() {
    std::cout << "* -------------------- FLOP -------------------- * [" << _money << "$]"<< std::endl;
    std::cout << std::endl;
    std::cout << " Flop: ";
    _cards[0].printCard();
    std::cout << "  ";
    _cards[1].printCard();
    std::cout << "  ";
    _cards[2].printCard();
    std::cout << std::endl;
    std::cout << "* ---------------------------------------------- *" << std::endl;
    std::cout << std::endl;
}
void Board::printTurn() {
    std::cout << "* -------------------- TURN -------------------- *[" << _money << "$]"<< std::endl;
    std::cout << std::endl;
    std::cout << " Turn: ";
    _cards[0].printCard();
    std::cout << "  ";
    _cards[1].printCard();
    std::cout << "  ";
    _cards[2].printCard();
    std::cout << "  ";
    _cards[3].printCard();
    std::cout << std::endl;
    std::cout << "* ---------------------------------------------- *" << std::endl;
    std::cout << std::endl;
}
void Board::printRiver() {
    std::cout << "* -------------------- RIVER -------------------- *[" << _money << "$]"<< std::endl;
    std::cout << std::endl;
    std::cout << " River: ";
    _cards[0].printCard();
    std::cout << "  ";
    _cards[1].printCard();
    std::cout << "  ";
    _cards[2].printCard();
    std::cout << "  ";
    _cards[3].printCard();
    std::cout << "  ";
    _cards[4].printCard();
    std::cout << std::endl;
    std::cout << "* ----------------------------------------------- *" << std::endl;
    std::cout << std::endl;
}
void Board::printBoard() {
    std::cout << "* -------------------- BOARD -------------------- *[" << _money << "$]"<< std::endl;
    std::cout << "  ";
    for (Card _c:_cards){
        _c.printCard();
        std::cout << "  ";
    }
    std::cout << std::endl;
    std::cout << "* ----------------------------------------------- *" << std::endl;
    std::cout << std::endl;
}


