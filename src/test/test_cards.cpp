/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <map>
#include <CppUTest/CommandLineTestRunner.h>
#include <vector>
#include "../main/Cards/Card.hpp"
#include "../main/Cards/EvalFunctions.hpp"
#include "../main/Cards/Box.hpp"
#include "../main/Game/Board.hpp"
#include "../main/Players/Player.hpp"
#include "../main/Game/Game.hpp"

TEST_GROUP(PokiaCard) {
};

TEST(PokiaCard, color) {
    Card _c1("2","*");
    CHECK_EQUAL("*",_c1.getColor());
}

TEST(PokiaCard, value) {
    Card _c1("2","*");
    CHECK_EQUAL("2",_c1.getValue());
}