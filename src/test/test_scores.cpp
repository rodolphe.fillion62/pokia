/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#include <map>
#include <CppUTest/CommandLineTestRunner.h>
#include <vector>
#include <iostream>
#include "../main/Cards/Card.hpp"
#include "../main/Cards/EvalFunctions.hpp"
#include "../main/Cards/Box.hpp"
#include "../main/Game/Board.hpp"
#include "../main/Players/Player.hpp"
#include "../main/Game/Game.hpp"

TEST_GROUP(PokiaScores) {
};

TEST(PokiaScores, pair) {
    std::vector<Card> _weakPair;
    _weakPair.push_back(Card("4","*"));
    _weakPair.push_back(Card("4","<3"));
    _weakPair.push_back(Card("10","^"));
    _weakPair.push_back(Card("K","*"));
    _weakPair.push_back(Card("Q","<3"));
    std::vector<Card> _strongPair;
    _strongPair.push_back(Card("4","<3"));
    _strongPair.push_back(Card("6","<>"));
    _strongPair.push_back(Card("6","^"));
    _strongPair.push_back(Card("8","*"));
    _strongPair.push_back(Card("9","^"));
    bool _condition = false;
    int _scoreWeakPair = std::get<1>(newGenerateHand(_weakPair));
    int _scoreStrongPair = std::get<1>(newGenerateHand(_strongPair));
    if (_scoreWeakPair < _scoreStrongPair){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, twoPair) {
    std::vector<Card> _weakPair;
    _weakPair.push_back(Card("Q","<>"));
    _weakPair.push_back(Card("7","^"));
    _weakPair.push_back(Card("7","<3"));
    _weakPair.push_back(Card("4","<3"));
    _weakPair.push_back(Card("8","^"));
    _weakPair.push_back(Card("J","*"));
    _weakPair.push_back(Card("4","<>"));
    std::vector<Card> _strongPair;
    _strongPair.push_back(Card("9","*"));
    _strongPair.push_back(Card("J","^"));
    _strongPair.push_back(Card("7","<3"));
    _strongPair.push_back(Card("4","<3"));
    _strongPair.push_back(Card("8","^"));
    _strongPair.push_back(Card("J","*"));
    _strongPair.push_back(Card("4","<>"));
    bool _condition = false;
    int _scoreWeakPair = std::get<1>(newGenerateHand(_weakPair));
    int _scoreStrongPair = std::get<1>(newGenerateHand(_strongPair));
    if (_scoreWeakPair < _scoreStrongPair){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, nothingInHand) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("A","*"));
    _weakHand.push_back(Card("K","<3"));
    _weakHand.push_back(Card("10","^"));
    _weakHand.push_back(Card("J","<3"));
    _weakHand.push_back(Card("2","<3"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("A","*"));
    _strongHand.push_back(Card("8","<3"));
    _strongHand.push_back(Card("10","*"));
    _strongHand.push_back(Card("K","^"));
    _strongHand.push_back(Card("J","^"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, twoPairOneKicker) {
    std::vector<Card> _weakPair;
    _weakPair.push_back(Card("A","*"));
    _weakPair.push_back(Card("A","<3"));
    _weakPair.push_back(Card("K","^"));
    _weakPair.push_back(Card("K","<3"));
    _weakPair.push_back(Card("10","<3"));
    std::vector<Card> _strongPair;
    _strongPair.push_back(Card("A","*"));
    _strongPair.push_back(Card("A","<3"));
    _strongPair.push_back(Card("K","*"));
    _strongPair.push_back(Card("K","^"));
    _strongPair.push_back(Card("J","^"));
    bool _condition = false;
    int _scoreWeakPair = std::get<1>(newGenerateHand(_weakPair));
    int _scoreStrongPair = std::get<1>(newGenerateHand(_strongPair));
    if (_scoreWeakPair < _scoreStrongPair){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, tok) {
    std::vector<Card> _weakTok;
    _weakTok.push_back(Card("A","*"));
    _weakTok.push_back(Card("A","<3"));
    _weakTok.push_back(Card("A","^"));
    _weakTok.push_back(Card("6","<3"));
    _weakTok.push_back(Card("5","<3"));
    std::vector<Card> _strongTok;
    _strongTok.push_back(Card("A","*"));
    _strongTok.push_back(Card("A","<3"));
    _strongTok.push_back(Card("A","<>"));
    _strongTok.push_back(Card("K","^"));
    _strongTok.push_back(Card("J","^"));
    bool _condition = false;
    int _scoreWeakTok = std::get<1>(newGenerateHand(_weakTok));
    int _scoreStrongTok = std::get<1>(newGenerateHand(_strongTok));
    if (_scoreWeakTok < _scoreStrongTok){
        _condition = true;

    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, pairOneKicker) {
    std::vector<Card> _weakTok;
    _weakTok.push_back(Card("A","*"));
    _weakTok.push_back(Card("A","<3"));
    _weakTok.push_back(Card("6","^"));
    _weakTok.push_back(Card("2","<3"));
    _weakTok.push_back(Card("5","<3"));
    std::vector<Card> _strongTok;
    _strongTok.push_back(Card("A","*"));
    _strongTok.push_back(Card("A","<3"));
    _strongTok.push_back(Card("10","<>"));
    _strongTok.push_back(Card("5","^"));
    _strongTok.push_back(Card("6","^"));
    bool _condition = false;
    int _scoreWeakTok = std::get<1>(newGenerateHand(_weakTok));
    int _scoreStrongTok = std::get<1>(newGenerateHand(_strongTok));
    if (_scoreWeakTok < _scoreStrongTok){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, straight) {
    std::vector<Card> _weakTok;
    _weakTok.push_back(Card("2","*"));
    _weakTok.push_back(Card("3","<3"));
    _weakTok.push_back(Card("4","^"));
    _weakTok.push_back(Card("5","<3"));
    _weakTok.push_back(Card("6","<3"));
    std::vector<Card> _strongTok;
    _strongTok.push_back(Card("4","*"));
    _strongTok.push_back(Card("5","<3"));
    _strongTok.push_back(Card("6","<>"));
    _strongTok.push_back(Card("7","^"));
    _strongTok.push_back(Card("8","^"));
    bool _condition = false;
    int _scoreWeakTok = std::get<1>(newGenerateHand(_weakTok));
    int _scoreStrongTok = std::get<1>(newGenerateHand(_strongTok));
    if (_scoreWeakTok < _scoreStrongTok){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, Whitestraight) {
    std::vector<Card> _weakTok;
    _weakTok.push_back(Card("A","*"));
    _weakTok.push_back(Card("3","<3"));
    _weakTok.push_back(Card("4","^"));
    _weakTok.push_back(Card("2","<3"));
    _weakTok.push_back(Card("5","<3"));
    std::vector<Card> _strongTok;
    _strongTok.push_back(Card("4","*"));
    _strongTok.push_back(Card("5","<3"));
    _strongTok.push_back(Card("6","<>"));
    _strongTok.push_back(Card("7","^"));
    _strongTok.push_back(Card("8","^"));
    bool _condition = false;
    int _scoreWeakTok = std::get<1>(newGenerateHand(_weakTok));
    int _scoreStrongTok = std::get<1>(newGenerateHand(_strongTok));
    if (_scoreWeakTok < _scoreStrongTok){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, fullHouse) {
    std::vector<Card> _weakTok;
    _weakTok.push_back(Card("A","*"));
    _weakTok.push_back(Card("A","<3"));
    _weakTok.push_back(Card("A","^"));
    _weakTok.push_back(Card("5","<3"));
    _weakTok.push_back(Card("5","*"));
    std::vector<Card> _strongTok;
    _strongTok.push_back(Card("A","*"));
    _strongTok.push_back(Card("A","<3"));
    _strongTok.push_back(Card("A","<>"));
    _strongTok.push_back(Card("6","*"));
    _strongTok.push_back(Card("6","<3"));
    bool _condition = false;
    int _scoreWeakTok = std::get<1>(newGenerateHand(_weakTok));
    int _scoreStrongTok = std::get<1>(newGenerateHand(_strongTok));
    if (_scoreWeakTok < _scoreStrongTok){
        _condition = true;

    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, CombinaisonsWithTwo) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("A","*"));
    _weakHand.push_back(Card("A","<3"));
    _weakHand.push_back(Card("K","^"));
    _weakHand.push_back(Card("K","<3"));
    _weakHand.push_back(Card("4","<3"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("2","*"));
    _strongHand.push_back(Card("2","<3"));
    _strongHand.push_back(Card("2","*"));
    _strongHand.push_back(Card("6","^"));
    _strongHand.push_back(Card("7","^"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, Egalite) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("A","<>"));
    _weakHand.push_back(Card("A","^"));
    _weakHand.push_back(Card("K","<>"));
    _weakHand.push_back(Card("K","<3"));
    _weakHand.push_back(Card("7","*"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("A","*"));
    _strongHand.push_back(Card("A","<3"));
    _strongHand.push_back(Card("K","*"));
    _strongHand.push_back(Card("K","^"));
    _strongHand.push_back(Card("7","^"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand == _scoreStrongHand){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}


TEST(PokiaScores, BugPair) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("2","^"));
    _weakHand.push_back(Card("7","<>"));
    _weakHand.push_back(Card("7","^"));
    _weakHand.push_back(Card("9","*"));
    _weakHand.push_back(Card("Q","<3"));
    _weakHand.push_back(Card("5","<>"));
    _weakHand.push_back(Card("A","^"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("4","<>"));
    _strongHand.push_back(Card("7","<3"));
    _strongHand.push_back(Card("7","^"));
    _strongHand.push_back(Card("9","*"));
    _strongHand.push_back(Card("Q","<3"));
    _strongHand.push_back(Card("5","<>"));
    _strongHand.push_back(Card("A","^"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, BugPair2) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("Q","<>"));
    _weakHand.push_back(Card("9","^"));
    _weakHand.push_back(Card("2","^"));
    _weakHand.push_back(Card("7","<>"));
    _weakHand.push_back(Card("J","*"));
    _weakHand.push_back(Card("7","<3"));
    _weakHand.push_back(Card("8","<>"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("K","<3"));
    _strongHand.push_back(Card("3","*"));
    _strongHand.push_back(Card("2","^"));
    _strongHand.push_back(Card("7","<>"));
    _strongHand.push_back(Card("J","*"));
    _strongHand.push_back(Card("7","<3"));
    _strongHand.push_back(Card("8","<>"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand){
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}

TEST(PokiaScores, BugPair3) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("10","<3"));
    _weakHand.push_back(Card("8","<>"));
    _weakHand.push_back(Card("A","*"));
    _weakHand.push_back(Card("3","<>"));
    _weakHand.push_back(Card("A","*"));
    _weakHand.push_back(Card("Q","<>"));
    _weakHand.push_back(Card("2","<>"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("Q","^"));
    _strongHand.push_back(Card("K","<>"));
    _strongHand.push_back(Card("A","*"));
    _strongHand.push_back(Card("3","<>"));
    _strongHand.push_back(Card("A","*"));
    _strongHand.push_back(Card("Q","<3"));
    _strongHand.push_back(Card("2","<>"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand) {
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}


TEST(PokiaScores, lastTok) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("A","<3"));
    _weakHand.push_back(Card("A","<>"));
    _weakHand.push_back(Card("A","*"));
    _weakHand.push_back(Card("A","<>"));
    _weakHand.push_back(Card("Q","*"));
    _weakHand.push_back(Card("Q","<>"));
    _weakHand.push_back(Card("2","<>"));
    CHECK_EQUAL(true,true);
}

TEST(PokiaScores, StraightFlush2) {
    std::vector<Card> _weakHand;
    _weakHand.push_back(Card("10","<3"));
    _weakHand.push_back(Card("9","<3"));
    _weakHand.push_back(Card("Q","<3"));
    _weakHand.push_back(Card("A","<3"));
    _weakHand.push_back(Card("7","<3"));
    _weakHand.push_back(Card("8","<3"));
    _weakHand.push_back(Card("6","<3"));
    std::vector<Card> _strongHand;
    _strongHand.push_back(Card("10","<3"));
    _strongHand.push_back(Card("9","<3"));
    _strongHand.push_back(Card("Q","<3"));
    _strongHand.push_back(Card("A","<3"));
    _strongHand.push_back(Card("7","<3"));
    _strongHand.push_back(Card("8","<3"));
    _strongHand.push_back(Card("J","<3"));
    bool _condition = false;
    int _scoreWeakHand = std::get<1>(newGenerateHand(_weakHand));
    int _scoreStrongHand = std::get<1>(newGenerateHand(_strongHand));
    if (_scoreWeakHand < _scoreStrongHand) {
        _condition = true;
    }
    CHECK_EQUAL(true,_condition);
}