/*
Pokia is a Texas Hold'em Poker game engine combined with IA.
Copyright (C) 2021 FILLION Rodolphe and HORELLOU Théo

Pokia is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Pokia is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pokia; see the file COPYING. If not, write to the
        Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#include <map>
#include "CppUTest/CommandLineTestRunner.h"
#include <vector>
#include <iostream>
#include "../main/Cards/Card.hpp"
#include "../main/Cards/EvalFunctions.hpp"
#include "../main/Cards/Box.hpp"
#include "../main/Game/Board.hpp"
#include "../main/Players/Player.hpp"
#include "../main/Game/Game.hpp"


TEST_GROUP(PokiaCombinaison) {

};

TEST(PokiaCombinaison, RFlush) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","<3"));
    _testCards.push_back(Card("K","<3"));
    _testCards.push_back(Card("Q","<3"));
    _testCards.push_back(Card("J","<3"));
    _testCards.push_back(Card("10","<3"));
    CHECK_EQUAL(true,isRFlush(_testCards, countCards(_testCards),_p));
}

TEST(PokiaCombinaison, Flush) {
    std::vector<Card> _testCards;
        double _p = 0;
    _testCards.push_back(Card("9","<3"));
    _testCards.push_back(Card("8","<3"));
    _testCards.push_back(Card("7","<3"));
    _testCards.push_back(Card("6","<3"));
    _testCards.push_back(Card("5","<3"));
    CHECK_EQUAL(true,isStraightFlush(_testCards, countCards(_testCards),_p));
}

TEST(PokiaCombinaison, Fok) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","<3"));
    _testCards.push_back(Card("A","*"));
    _testCards.push_back(Card("A","<>"));
    _testCards.push_back(Card("A","^"));
    _testCards.push_back(Card("10","<3"));
    CHECK_EQUAL(true,isFok(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, FH) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","<3"));
    _testCards.push_back(Card("A","*"));
    _testCards.push_back(Card("A","<>"));
    _testCards.push_back(Card("K","<3"));
    _testCards.push_back(Card("K","*"));
    CHECK_EQUAL(true,isFH(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, Color) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","<3"));
    _testCards.push_back(Card("10","<3"));
    _testCards.push_back(Card("8","<3"));
    _testCards.push_back(Card("5","<3"));
    _testCards.push_back(Card("2","<3"));
    CHECK_EQUAL(true, isFlush(_testCards,_p));
}

TEST(PokiaCombinaison, WhiteStraight) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("4","<3"));
    _testCards.push_back(Card("5","<3"));
    _testCards.push_back(Card("2","*"));
    _testCards.push_back(Card("3","<>"));
    _testCards.push_back(Card("A","^"));
    CHECK_EQUAL(true,isStraight(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, Straight) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("6","<3"));
    _testCards.push_back(Card("5","<3"));
    _testCards.push_back(Card("4","*"));
    _testCards.push_back(Card("3","<>"));
    _testCards.push_back(Card("2","^"));

    CHECK_EQUAL(true,isStraight(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, TOK) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("4","*"));
    _testCards.push_back(Card("4","<3"));
    _testCards.push_back(Card("4","^"));
    _testCards.push_back(Card("K","*"));
    _testCards.push_back(Card("Q","<3"));
    CHECK_EQUAL(true,isTok(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, TwoPair) {
    double _p = 0;
    std::vector<Card> _testCards;
    _testCards.push_back(Card("A","<3>"));
    _testCards.push_back(Card("A","*"));
    _testCards.push_back(Card("K","<3"));
    _testCards.push_back(Card("K","*"));
    _testCards.push_back(Card("5","*"));
    CHECK_EQUAL(true,isTwoPair(countCards(_testCards),_p));
}

TEST(PokiaCombinaison, Pair) {
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","<3"));
    _testCards.push_back(Card("A","*"));
    _testCards.push_back(Card("6","<3"));
    _testCards.push_back(Card("4","<3"));
    _testCards.push_back(Card("2","<>"));
    CHECK_EQUAL(true,isPair(countCards(_testCards),_p));
}

TEST(PokiaCombinaison,BugComplex){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("8","^"));
    _testCards.push_back(Card("8","<3"));
    _testCards.push_back(Card("5","<>"));
    _testCards.push_back(Card("8","*"));
    _testCards.push_back(Card("K","<3"));
    _testCards.push_back(Card("2","^"));
    _testCards.push_back(Card("4","<3"));
    CHECK_EQUAL(true,isTok(countCards(_testCards),_p));
}

TEST(PokiaCombinaison,BugComplex2){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("8","^"));
    _testCards.push_back(Card("8","<3"));
    _testCards.push_back(Card("8","<>"));
    _testCards.push_back(Card("Q","*"));
    _testCards.push_back(Card("K","<3"));
    _testCards.push_back(Card("2","^"));
    _testCards.push_back(Card("4","<3"));
    CHECK_EQUAL(true,isTok(countCards(_testCards),_p));
}

TEST(PokiaCombinaison,BugComplex3){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("K","^"));
    _testCards.push_back(Card("6","^"));
    _testCards.push_back(Card("9","*"));
    _testCards.push_back(Card("3","<>"));
    _testCards.push_back(Card("9","$ "));
    _testCards.push_back(Card("6","*"));
    _testCards.push_back(Card("K","<3"));

    CHECK_EQUAL(true,isTwoPair(countCards(_testCards),_p));
}


TEST(PokiaCombinaison,BugComplex4){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("2","^"));
    _testCards.push_back(Card("2","^"));
    _testCards.push_back(Card("3","*"));
    _testCards.push_back(Card("7","<>"));
    _testCards.push_back(Card("9","$ "));
    _testCards.push_back(Card("3","*"));
    _testCards.push_back(Card("K","<3"));
    CHECK_EQUAL(true,isTwoPair(countCards(_testCards),_p));
}

TEST(PokiaCombinaison,BugComplex5){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("10","<3"));
    _testCards.push_back(Card("8","<>"));
    _testCards.push_back(Card("10","*"));
    _testCards.push_back(Card("10","^"));
    _testCards.push_back(Card("6","^ "));
    _testCards.push_back(Card("2","<3"));
    _testCards.push_back(Card("6","<3"));
    CHECK_EQUAL(true,isTok(countCards(_testCards),_p));
}

TEST(PokiaCombinaison,BugComplex6){
    std::vector<Card> _testCards;
    double _p = 0;
    _testCards.push_back(Card("A","*"));
    _testCards.push_back(Card("6","^"));
    _testCards.push_back(Card("6","<3"));
    _testCards.push_back(Card("4","<3"));
    _testCards.push_back(Card("9","<3 "));
    _testCards.push_back(Card("10","<>"));
    _testCards.push_back(Card("A","<3"));
    std::cout << std::endl;
    CHECK_EQUAL(true,isTwoPair(countCards(_testCards),_p));
}


